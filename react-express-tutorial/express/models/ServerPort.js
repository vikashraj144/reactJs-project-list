var mongoose = require('mongoose');
var schema = mongoose.Schema;

var serverPort = new schema({
    name: {
        type: String
    },
    port: {
        type: Number
    }
}, 
{ 
    versionKey: false
  }
);

module.exports = mongoose.model('serverPort', serverPort);