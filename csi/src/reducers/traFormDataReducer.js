import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function traFormDataReducer(state = initialState.traFormData, action) {

    switch(action.type) {
        case types.LOAD_ALL_TRA_FORM_DATA:
        //Object.assign() is added to aviod redux state mutation
        return Object.assign({},state,{
            ...action.traFormData
        });
        default:
            return state;
    }
}