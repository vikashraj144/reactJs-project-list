import { combineReducers } from 'redux';
import allDataReducer from './allDataReducer';
import sepFormDataReducer from './sepFormDataReducer';
import traFormDataReducer from './traFormDataReducer';
import ifscFormDataReducer from './ifscFormDataReducer';
import aahFormDataReducer from './aahFormDataReducer';

const rootReducer = combineReducers({
    allData:        allDataReducer,
    sepFormData:    sepFormDataReducer,
    traFormData:    traFormDataReducer,
    ifscFormData:   ifscFormDataReducer,
    aahFormData:    aahFormDataReducer
});

export default rootReducer;