import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function aahFormDataReducer(state = initialState.aahFormData, action) {

    switch(action.type) {
        case types.LOAD_AAH_FORM_DATA:
        //Object.assign() is added to aviod redux state mutation
        return action.aahFormData
        default:
            return state;
    }
}