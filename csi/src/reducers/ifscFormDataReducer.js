import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function ifscFormDataReducer(state = initialState.ifscFormData, action) {

    switch(action.type) {
        case types.LOAD_ALL_IFSC_FORM_DATA:
        //Object.assign() is added to aviod redux state mutation
        return Object.assign({},state,{
            ...action.ifscFormData
        });
        default:
            return state;
    }
}