import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function sepFormDataReducer(state = initialState.sepFormData, action) {

    switch(action.type) {
        case types.LOAD_ALL_SEP_FORM_DATA:
        //Object.assign() is added to aviod redux state mutation
        return Object.assign({},state,{
            ...action.sepFormData
        });
        default:
            return state;
    }
}