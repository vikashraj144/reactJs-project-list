import axios from 'axios';

// header config
export const headerInfo = {
    headers: { 
            'Content-Type': 'application/x-www-form-urlencoded',
            'clientId':e1clientId,
            'selectedBU':'CTI',
            'clientSecret':e1clientSecret,
            /* 'SMSESSION':"QXqg4ilWV1waXLwePDqymuxl55Ow2yrfXkdSs77ekJiwUKsaZ0rUafYFN9Jf9BcOW7a1+05u/7DmTgwbiC4evFuH8zuGyLminbFr6q5311f+3K5YKlTOqM7YgGpL2hZzmO4AsP8KYiGhsxRKluYQbk5JiIZDNki22x0fHcl3jQJOSkmQl6M9w3DN94uRw05udfEa5NQsGeOwopvGEKmVN85HsAtjAdUaW3r4aqvTuOPK2Hk8bQMNag3ACSNSMlJJxZZ9bV7VPDFBYCADDXTej3CVB9akCwoZ/9K/VYyujighY4YwDIcOBg17pECOnc5t/QW468EtmLGWYeYoUXUg+l6KAjqOQx1NUjRAjAcqJiCH3aZYxkWdZm8MLBcLgoYuj9EA+aOIuPyZFhy13PYmhRzVUiSDM2TAMh+rHHiqEgLJJqwwuZoIZRwmzNnIM8beGMHXRmYMV30KkzVCcrGXLxSqJv7/M+atOHUHySDRxkGVyDfRChspmkCn29e53U28tDcHg4VclLUb8hQTO5N9loP7v02Ov/NlR4DSqJO7aYRal6s43GsMAr/9+bZqr4LYx0XlH6t6dJUDeZTJupPPEXmz4cH1yHrfnZlOmk80RAaZgJO//wdqoqJivi2AaDTxiq4sooHnTS9PxC7g3qie127/q6U5E03m1gpVC2Hg7CcR4xw9T/ydsK2YXv5OaGifACsH5ZnLJcDKrT2XwnuY8LbMsdotWd1tunJswzR82S86t/3hWCW8zLLcCVfYVcozuCW99e5s7NSjS7772tQWOUrYcjZpdV23mbTpxGuHPSf/IXnZPTsyyWl23dTMRTfogU1o30Wnk7rGikEFbq6oOUiN2nvfWbi8hII+GW3ijvri93qBf/wBQ+wcIKPaYwaU6plYLJ1+JrmkBT2Lu4VJINt14/cRc/9zOU2uqWrHrhJMiyFnj8Ai0eNrMDTdcc05bxnyyXsR7Xl5gTW4/FL31DWkYwQOMIBKA4LyeDxy9y+NJ8Yru8rj+yjCaql+hihQ4Fw5gjsANLMu+imdg1Glj+V12/5raVzE3xwdYkGk0kktCwukHWUTkg==", */
    },
    responseType: 'blob'
  }

// base url setup remove these and put in environment variable
export const baseURL = 'http://localhost:3000/json/';
export const e1BaseURL = 'https://api.wamac.dev.ask.com';
export const e1clientId = '41eb2d1e-d65c-4d14-bf23-8ebf94bd3f0a-99999999';
export const e1clientSecret = 'a213c287-4386-4e51-8f1f-d186172335fb-00000000';


export const ERROR_MSGS = {
    url : axios.create({
            baseURL: 'http://localhost:3000/json'
          }),
    SSN_LENGTH_ERROR : "SSN can't be more than 9 characters",
    SSN_VALIDATION_ERROR: "SSN entered is not valid",
    INVALID_EMAIL:"Invalid Email ID",
    ENTER_EMAIL_SSN:"Invalid Email ID",

    CLIENTID_LENGTH_ERROR: "ClientID Number must contain 12 numbers",
    CLIENTID_VALIDATION_ERROR: "ClientID Number is invalid",
    USER_ONLY_VALIDATION:"Enter User ID",
    USER_OR_GUID_VALIDATION:"Enter User ID or GUID",
    FULLNAME_VALIDATION:"Full name can not be blank",
    NAME_VALIDATION:"Name can not be blank",
    NAME_INVALIDATION:"Name must contain only alpha characters",
    USER_ID_VALIDATION_ERROR: "User ID must contain 5-20 alphanumeric or special characters.  The following characters are not permitted \" \" ' & < > / \ * ? ) ( ^ blank ",
    USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR: "User Name must contain at least one alpha character and one number",
    GUID_LENGTH_ERROR: "GUID must be 32 characters",
    GUID_VALIDATION_ERROR: "GUID is not valid",
    ENTER_DETAILS_VALIDATION_ERROR: "Please enter details",
    MYFA_NO_FIELD_ERROR: "Enter SSN or UserID or ClientID or GUID",
    ENROLL_ACCEPT_DEFAULT_ERROR: "The primary email address entered is also the primary email address for another client's Message Center. If the client chooses to continue registering this email address you can click accept to complete registration to the secure site, they will be unable to send or receive secure messages through Message Center.",
    ONLY_CHARCTER_VALIDATION:"Full Name must contain only alpha characters",
    ENROLL_SUCCESS:"Enroll operation Successful",
    UNSUBSCRIBE_SUCCESS:"Unsubscribe operation Successful",
    UPDATE_SUCCESSFULL:"Update Successful"
}