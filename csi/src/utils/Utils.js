
/**
 * File to add Utilities that are 
 * common and can be used at multiple places
 */
export default class Utils {

    /**
     * Function to accept only Numeric characters
     */
    static regExNumber = (value) => {
        let regEx = /^[0-9]*$/;
        return regEx.test(value);
    }

    /**
     * Function to trim spaces
     */
    static trimSpaces = (value) => {
        return value.replace(/\s/g, "");
    }

    /**
     * Function to validate Alphanumeric with special characters
     */
    static regExAlphaNumeric = (value) => {
        let regEx = /^[(!@#$-%^)a-zA-Z0-9 \s]*$/;
        return regEx.test(value);
    }

     /**
     * Function to validate Alphanumeric characters
     */
    static regExAlphaNumericOnly = (value) => {
        let regEx = /^[a-zA-Z0-9 \s]*$/;
        return regEx.test(value);
    }

     /**
     * Function to validate Alphabets characters
     */
    static regExAlphabetsOnly = (value) => {
        let regEx = /^[a-zA-Z \s]*$/;
        return regEx.test(value);
    }
    
    /**
     * Function to validate atleast one Alphabets and one Character characters
     */
    static regExOneAlphabetsAtleastOneNumber = (value) => {
        let regEx = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;
        return regEx.test(value);
    }
    /**
     * Function to validate atleast one Alphabets and one Character characters
     */
    static emailRegExp = (value) => {
        var emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRegExp.test(value);
    }
    /**
     * Function to add loader
     */
    static pLoader = () => {
        let x = document.createElement("div");
        x.className = ('pageLoader');
        x.innerHTML= "<div class='lds-roller'>"+
                     "<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>"+
                     "</div>";
        document.body.appendChild(x);
        //closeLoader();
    }
    
    /**
     * Function to close loader
     */
    static closeLoader = () => {
        /* setTimeout(function()
        { */
            let el = document.querySelector( '.pageLoader' );
            if(el){
                el.parentNode.removeChild( el );
            }
        //}, 3000);
    }
}
