import React, { Component } from "react";
import {
  Route,
  BrowserRouter,
  Redirect

} from "react-router-dom";

import Contact from "./Contact";
import Header from './Header.js';
import Footer from './Footer.js';

import PageNotFound from "./components/PageNotFound";

import Sep from "./components/sep/Sep";
import SepProfile from "./components/sep/SepProfile";
import EnrollSep from "./components/sep/EnrollSep";

import Pap from "./components/pap/Pap";
import PapProfile from "./components/pap/PapProfile";
import Aah from "./components/aah/Aah";
import AahProfile from "./components/aah/AahProfile";
import AahUser from "./components/aah/AahUserList";
import EnrollAah from "./components/aah/EnrollAah";
import * as cc90File from './components/cc90';
import * as ifscFile from './components/ifsc';
import * as TraFile from "./components/tr";
import * as MyfaFile from './components/myfa';
import Utils from './utils/Utils';
import ConfigService from './api/ConfigService';

// const customHandleClick = (param) => {
// 	showSubmenu: true;
// 	if(!param){
// 		showSubmenu:true;
// 	}
// 	console.log(param)
// }

class Main extends Component {
  
  	componentDidMount = () => {

		Utils.pLoader();
		ConfigService.getSession();
		// ConfigService.getToken();
		Utils.closeLoader();
	}
	
	render() {
		return (
			<BrowserRouter>
				<div className="wrapper">			
					<Header />
						<div className="b_content">
				{/* <Redirect from="/csi" to="csi" /> */}

				<Route path='/bsi' component={MyfaFile.Myfa} exact/>

				<Route path='/csi' component={MyfaFile.Myfa} exact/>
				<Route path='/csi/myfa-ssn-guid-auth' component={MyfaFile.SsnGuidAuth} />
				<Route path='/csi/myfa-client-number-auth' component={MyfaFile.ClientNumberAuth} />
				<Route path='/csi/myfa-snn-tin-number-auth' component={MyfaFile.SnnTinAuth} />
				<Route path='/csi/myfa/:profile' component={MyfaFile.MyfaProfile} />
				{/* duplicate of profile */}
				<Route path='/csi/myfa-profile-subscribe' component={MyfaFile.MyfaProfileSubscribe} />
				<Route path='/csi/myfa-enroll-new' component={MyfaFile.EnrollMyfa} />              
				<Route path='/csi/myfa-change-user-id' component={MyfaFile.ChangeUserId} />              


				<Route path='/csi/tra' component={TraFile.Tra} exact/>
				<Route path='/csi/tra/profile' component={TraFile.TraProfile} exact/>
				<Route path='/csi/tra/profileSub' component={TraFile.TraProfileSubscribe} exact/>
				{/* <Route path='/tra-enroll-new' component={EnrollTra} /> */}


				<Route path='/csi/aah' component={Aah} exact/>
				<Route path='/csi/aah/profile' component={AahProfile} />
				<Route path='/csi/aah-enroll-new' component={EnrollAah} />
				<Route path='/csi/aah-user-list' component={AahUser} />
				{/* <Route path='/rvs' component={rvs}/> */}

				<Route path='/csi/ifsc' component={ifscFile.Ifsc} exact/>
				<Route path='/csi/ifsc/profile' component={ifscFile.IfscProfile} />
				<Route path='/csi/ifsc/user-list' component={ifscFile.IfscUserList} />

				<Route path='/csi/sep' component={Sep} exact/>
				<Route path='/csi/sep/:profile' component={SepProfile} />
				<Route path='/csi/sep-enroll-new' component={EnrollSep} />

				<Route path='/csi/pap' component={Pap} exact/>
				<Route path='/csi/pap/:profile' component={PapProfile} />
				
				<Route path='/csi/cc90' component={cc90File.cc90} exact/>
				<Route path='/csi/cc90/:profile' component={cc90File.cc90Profile}/>
				<Route path='/csi/cc90-enroll-new' component={cc90File.EnrollNewCc90}/>

				<Route path={'/contact'} component={Contact}/>
				<Route path={'/404Error'} component={PageNotFound}/>

						</div>
					<Footer />
				</div>
			</BrowserRouter>	
		);
  	}
}

 
export default Main;