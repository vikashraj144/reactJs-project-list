import 'core-js/es6/map';
import 'core-js/es6/set';
import 'core-js/es6/number';
import 'core-js/es6/array';


import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";
import "./resources/css/main.css";
//To add REDUX
import registerServiceWorker from "./registerServiceWorker";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";

let store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Main/>
  </Provider>,
  document.getElementById("root")
  
);
registerServiceWorker();