import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (	 
      <footer ref="footer">Copyright &copy; 2018 Ameriprise Financial, Inc.</footer>	  
    );
  }
}
export default Footer;