import * as types from './actionTypes';

/* 
Actions are payloads of information that send data from your application to your store
*/
/**
 * Function to get all data
 * @param {Object Array} allData 
 */
export const loadAllData = (allData) => {
    
    return {
        type: types.GET_ALL_DATA,
        allData
    }
}

export const loadSepFormData = (sepFormData) =>{
    return {
        type:types.LOAD_ALL_SEP_FORM_DATA,
        sepFormData
    }
}

export const loadTraFormData = (traFormData) =>{
    return {
        type:types.LOAD_ALL_TRA_FORM_DATA,
        traFormData
    }
}

export const loadIfscFormData = (ifscFormData) =>{
    return {
        type:types.LOAD_ALL_IFSC_FORM_DATA,
        ifscFormData
    }
}

export const loadAAHFormData = (aahFormData) => {
    return {
        type: types.LOAD_AAH_FORM_DATA,
        aahFormData
    }
}