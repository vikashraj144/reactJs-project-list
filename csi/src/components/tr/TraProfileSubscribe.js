import React,{Component} from "react";
// import InputMessageBar from "../shaired/InputMessageBar";
import axios from 'axios';

import MessageBar from "../shaired/MessageBar";
import * as Constants from '../../utils/Constants';

class TraProfileSubscribe extends Component{

    constructor(props){
        super(props);
        this.state = {
            class:'',
            errorMsg:'',
            pass:'',
            disabled:true,
            readOnly: true,
            revokeLable:'Revoke',
            displayClass:'display-non',
        };
        this._onCheck = this._onCheck.bind(this);
    }

    _onCheck = () => {
    this.setState(prevState => ({readOnly: !prevState.readOnly,disabled: !prevState.disabled}))
    if(this.state.readOnly=== true){
    this.setState({displayClass:""});
    }else{
    this.setState({displayClass:"display-non"});
    }
  }

  componentWillMount() {
      if(!this.props.location.state){
          this.onMainSearch();
      }
  }
    onMainSearch = () => {
        this.props.history.push('/csi/tra');
    }
    
    toggleSubscribe = () => {
        this.setState({class:'',errorMsg: ""});
    }

    
    onResetPassword = (event) =>{
        if (window.confirm("Are you sure you want to reset password?")) { 
            this.setState({class:'error',errorMsg:"Reset Password Successful - New password is: ",pass:'! 3 4 5 6 $ % #'});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();    
    }

    onSubscribe = () => {
        const param = {
            "userGuid": "srajap1xxxxxxxxxxxxxxxx32432Z",
            "TRRoleType": "Super Admin",
            "BRMRoleType": "BRM view",
            "firstName": "vikash",
            "lastName": "verma",
            "employeeNumber": "326835",
            "emailAddress": "vverma14@ampf.com",
            "subscribedServicesCd" : "asd, 123,5456"
        }
         // get data from json
      axios.get( Constants.baseURL+'tra/trAdmin_subscribe_success_response.json',param,Constants.headerInfo)
      .then((response) =>  {
        console.log("tra subscribe data-----", response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
        this.props.history.push('/csi/tra/profile');
    }

    onUpdate = () => {
        this.setState({class:'success',errorMsg:"Update Successful",pass:''});

    }

    onRevoke = () =>{
        let revokeLableVal = (this.state.revokeLable ==='Revoke')?'Reinstate':'Revoke';
        let revokeMsg = (this.state.revokeLable ==='Revoke')?'Revoke Successful':'Reinstate Successful';
        this.setState({class:'success',errorMsg:revokeMsg,pass:'',revokeLable:revokeLableVal,displayClass:"display-non"});
    }

    render(){
        const isError = this.state.class;
        let message;
        let adminInfo;
        if(this.state.pass!=''){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }else if((isError === 'error' || isError==='success') && this.state.pass ===''){
            adminInfo = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
        }

        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> TRA Customer Information</h1>
            </div>    
                <div className="profileForms">
                        {/* 1st tab start */}
                    <table>
                        <tbody>
                    <tr className="profile-head-color">
                        <th>Security Identity Data</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td className="bold td1">User ID:</td>
                        <td className="">{this.props.location.state?this.props.location.state.userId:'david'}</td> 
                    </tr>
                    <tr>
                        <td className="bold td1">GUID:</td>
                        <td className="">34567</td> 
                    </tr>
                    </tbody>
                    </table>
                    
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            
            <div className="divTable">
            <table>
                <tbody>
                    <tr className="profile-head-color">
                        <th>TR Admin Information</th>
                    </tr>
                <tr>
                    <td colSpan="2">
            {adminInfo}
                    </td>
                </tr>
                    <tr>
                        <td className="bold tra-td1">TR Role Type:</td>
                        <td className="td2"> 
                        <select className="select-tra">
                            <option value="none">Select TR Role Type</option>
                            <option value="SuperAdmin">Super Admin</option>
                            <option value="RegularAdmin">Regular Admin</option>
                            <option value="ComplianceAdmin">Compliance Admin</option>
                        </select>
                        {/* <p>
                        <span className="tr-span-check">
                         Edit Profile <input type="checkbox" onChange={this._onCheck} />
                        </span>
                        </p> */}
                        </td> 
                        <td className="td3">
                        {/* <span className={(this.state.revokeLable==="Reinstate")?this.state.displayClass:''}>
                        Edit Profile <input type="checkbox" onChange={this._onCheck} />

                        </span> */}
                        </td>
                    </tr>
                    <tr>
                        <td className="bold tra-td1">BRM Role Type:</td>
                        <td className="td2"> 
                        <select className="select-tra">                        
                            <option value="none">None</option>
                            <option value="BRMView">BRMView</option>
                            <option value="BRMAdmin">BRMAdmin</option>
                            <option value="BRMView">BRMManager</option>
                        </select></td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">First Name:</td>
                        <td className="td2">
                           <input type="text" disabled className="enroll trProfileInput" value="David" readOnly={this.state.readOnly}/> 
                        </td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">Last Name:</td>
                        <td className="td2">
                        <input type="text" disabled className="enroll trProfileInput" value="bowie" readOnly={this.state.readOnly}/> 
                        </td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">Employee Number:</td>
                        <td className="td2">
                        <input type="text" disabled className="enroll trProfileInput" value="234567" readOnly={this.state.readOnly}/>                                                 
                        </td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">E-mail:</td>
                        <td className="td2">
                        <input type="text" disabled className="enroll trProfileInput" value="devidbowie@ampf.com" readOnly={this.state.readOnly} />                         
                        </td> 
                    </tr>
                    <tr>
                        <td>

                        </td>
                    </tr>
                    </tbody>
                    </table>
                                    <center>
                                    <button onClick={this.onSubscribe}>Subscribe</button>
                            </center>
            </div>
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
                {message}
                <form>
                 <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button onClick={this.onMainSearch}>Main Search</button>
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}

export default TraProfileSubscribe;