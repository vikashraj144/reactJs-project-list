import React, { Component } from "react";
import axios from 'axios';
import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
import Utils from '../../utils/Utils';
import * as Constants from '../../utils/Constants';

// import * as searchData from './../../json/tra/tra-search.json'
import { connect } from 'react-redux';
// import ConfigService from "../../api/ConfigService";
import { bindActionCreators } from 'redux';
import { loadTraFormData } from "./../../actions/dataAction"

class Tra extends Component {
   
constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'TRA',
        userId:'',
        class:'error',
        isDisable:false,
        errorMsg:'',
        validated:false,
		    traForm : {}
    }
    
    this.userIDhandleChange = this.userIDhandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  /* 
    intializing traForm and setting in state
  */
  componentDidMount = () =>{
    console.log('componentDidMount');

    localStorage.removeItem('userId');
    let traForm = this.state.traForm;
    traForm.userId = ''
    this.setState({traForm});
  }

  /**
	 * Function to handle User ID input
	 */
	userIDhandleChange = (event) => {
		// Removing spaces
    let userIDValue = Utils.trimSpaces(event.target.value);
    let disableGUIDVal = (event.target.value.length)?true:false;
    
    let traForm = this.state.traForm;
		traForm.userId = event.target.value;
    this.setState({traForm})
    
    this.setState({disableGUID:disableGUIDVal });
    let valid = Utils.regExAlphaNumeric(userIDValue);
		if(valid) {
			if (userIDValue.length >= 5 && userIDValue.length <= 20) {
        this.setState({ validated: true, class:'', errorMsg:''});

        if(!Utils.regExOneAlphabetsAtleastOneNumber(userIDValue)){
				  this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR, validated: false });
        }                
			} else if (userIDValue.length === 0) {
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION, validated: false });
			} else {
				this.setState({ class: 'error', validated: false,
					errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
			}
		} else {
			this.setState({ validated: false });
		}
  }
  
  onSearch = (event) =>{
    event.preventDefault();    
    if(this.state.traForm.userId.length === 0 ){
      this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION});
      event.preventDefault();    
    }else if(this.state.validated){
      const param = {
        userId:this.state.userId
      }
      // get data from json
      axios.get( Constants.baseURL+'tra/tra-search.json',param,Constants.headerInfo)
      .then((response) =>  {
        console.log("tra-----", response.data);
        let resTraForm = response.data;
        this.props.loadTraFormData(resTraForm);
      })
      .catch(function (error) {
        console.log(error);
      });

      window.localStorage.setItem('userId',this.state.traForm.userId);

      if(this.state.traForm.userId === 'vverma14'){
        this.props.history.push('/csi/tra/profileSub',{userId:this.state.traForm.userId})
      }else{
        this.props.history.push('/csi/tra/profile')
      }
    }
  }

  onReset = (event) => {
    let traForm = this.state.traForm;
    traForm.userId = ''
    this.setState({traForm,errorMsg:''})
    event.preventDefault();    
  }

  render() {
    return (
      <div>
        <HeaderTab />
       <div className="bauSelect align-center">
          <strong>&raquo; </strong><b>Current Servicing Type</b> <strong>-</strong> {this.state.currentServicingType}            
       </div>
        <div className="bauForms">
           <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p>
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
           <br></br>
              <form id='userForm'>
               <input type="text" placeholder="User ID" value={this.state.traForm.userId} onChange={this.userIDhandleChange} /> 
               <div className="bttns">
                  <button onClick={this.onSearch} disabled={this.state.isDisable}>Search</button>
                  <button onClick={this.onReset}>Clear</button>
               </div>   
            </form>

        </div>
        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
	return {
		traFormData: state.traFormData
	}	
};


const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadTraFormData
}, dispatch);
    // export default Tra;
export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(Tra);
