import React,{Component} from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';

import MessageBar from "../shaired/MessageBar";
// import InputMessageBar from "../shaired/InputMessageBar";
import * as Constants from '../../utils/Constants';

import { loadTraFormData } from "./../../actions/dataAction"
import Axios from "axios";

// var _ = require('lodash');
class TraProfile extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            class:'',
            errorMsg:'',
            pass:'',
            disabled:true,
            readOnly: true,
            revokeLable:'Revoke',
            displayClass:'display-non',

            firstname:'',
            lastname :'',
            employeeNumber :'',
            email :'',
            BRMRoleType :'',
            TRRoleType :'',
            userId:'',
            userGuid:'',
            isSubscribed:false,
        };
        this._onCheck = this._onCheck.bind(this);
    }

    _onCheck = () => {
    this.setState(prevState => ({readOnly: !prevState.readOnly,disabled: !prevState.disabled}))
    if(this.state.readOnly=== true){
    this.setState({displayClass:""});
    }else{
    this.setState({displayClass:"display-non"});
    }
  }

  componentDidMount(){
    let userId = window.localStorage.getItem('userId');
    if(userId){
         if(localStorage.getItem('userId').length){
            const param = {
                userId:userId
            }
            Axios.get( Constants.baseURL+'tra/tra-search.json',param,Constants.headerInfo)
            .then((response) =>  {
                let resTraForm = response.data;
                this.updateTrForm(resTraForm);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
     }else{
         this.onMainSearch()
     }
  }

    componentWillReceiveProps = (nextProps) =>{
        const {traFormData} = nextProps;
        if(this.state)
        this.updateTrForm(traFormData);
    }

    updateTrForm = (traFormData) => {
        if(traFormData.identityProfile){
            traFormData.identityProfile.map(a=>{
                this.setState({[a.param]:a.value});
            })
        }
            
        if(traFormData.businessProfile){
            traFormData.businessProfile.map(d=>{
                this.setState({[d.param]:d.value});
            })
        }
    }

    onMainSearch = () => {
        this.props.history.push('/csi/tra');
    }
    
    toggleSubscribe = () => {
        this.setState({class:'',errorMsg: ""});
    }

    onResetPassword = (event) =>{
        if (window.confirm("Are you sure you want to reset password?")) { 
            this.setState({class:'error',errorMsg:"Reset Password Successful - New password is: ",pass:'! 3 4 5 6 $ % #'});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();    
    }

    onUpdate = () => {
        const param = {
            TRRoleType:this.state.TRRoleType,
            BRMRoleType:this.state.BRMRoleType
        }
        //hit update api and handel response 
        axios.get( Constants.baseURL+'tra/editProfile_successResponse.json',param,Constants.headerInfo)
        .then((response) =>  {
          console.log("tra-----", response.data);
          let resTraForm = response.data;
          // dispatch to redux
          this.props.loadTraFormData(resTraForm);
        })
        .catch(function (error) {
          console.log(error);
        });
        // response to set state
        this.setState({class:'success',errorMsg:Constants.ERROR_MSGS.UPDATE_SUCCESSFULL,pass:''});
    }

    trRoleTypeChange = (event) => {
        this.setState({TRRoleType:event.target.value})
    }

    brmRoleTypeChange = (event) =>{
        this.setState({BRMRoleType:event.target.value})
    }

    onClear = () => {
        console.log('data from store', this.props.traFormData);
        
        this.updateTrForm(this.props.traFormData);
        // this.setState({TRRoleType:'',BRMRoleType:''});
    }

    onRevoke = () =>{
        // param is TRadmin_Revoke_Request.json and TRAdmin_Reinstate_Request.json
        let param = {
            userGuid:this.state.userGuid
        }
        let revokeReInstantUrl;
        if(this.state.revokeLable ==='Revoke'){
            revokeReInstantUrl = Constants.baseURL+'tra/Revoke_successResponse.json'; 
        }else{
            revokeReInstantUrl = Constants.baseURL+'tra/Reinstate_successResponse.json'; 
        }
        axios.get(revokeReInstantUrl,param,Constants.headerInfo)
        .then((response) => {
            let resRevoke = response.data;
            if(resRevoke.status === "success" ){
                console.log("tra---revoke--", response.data);
                
                let revokeLableVal = (this.state.revokeLable ==='Revoke')?'Reinstate':'Revoke';
                let revokeMsg = (this.state.revokeLable ==='Revoke')?'Revoke Successful':'Reinstate Successful';
                this.setState({class:'success',errorMsg:revokeMsg,pass:'',revokeLable:revokeLableVal,displayClass:"display-non"});
            
            }
        }).catch( (error) => {
            console.log(error);
        })
    }

    render(){

        // console.log(this.state.isSubscribed,'this.state.isSubscribed');
        
        const isError = this.state.class;
        let message,adminInfo,userID;
        if(this.state.pass!=''){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }else if((isError === 'error' || isError==='success') && this.state.pass ===''){
            adminInfo = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
        }
        // if(this.props.location.state){
        //     userID = this.props.location.state.userId.length?this.props.location.state.userId:this.state.userId
        // }
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> TRA Customer Information</h1>
            </div>    
                <div className="profileForms">
                        {/* 1st tab start */}
                    <table>
                        <tbody>
                    <tr className="profile-head-color">
                        <th>Security Identity Data</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td className="bold td1">User ID:</td>
                        <td className="">
                        {this.state.userId}
                        </td> 
                    </tr>
                    <tr>
                        <td className="bold td1">GUID:</td>
                        <td className="">{this.state.userGuid}</td> 
                    </tr>
                    </tbody>
                    </table>
                    
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            
            <div className="divTable">
            <br></br>
            {adminInfo}
            <table>
            <tbody>
                    <tr className="profile-head-color">
                        <th>TR Admin Information</th>
                    </tr>
                <tr>
                    <td colSpan="2">
                    </td>
                </tr>
                    <tr>
                        <td className="bold tra-td1">TR Role Type:</td>
                        <td className="td2"> 
                        <select className="select-tra" value={this.state.TRRoleType} disabled={this.state.disabled} onChange={this.trRoleTypeChange}>
                            <option value="none">Select TR Role Type</option>
                            <option value="SuperAdmin">Super Admin</option>
                            <option value="RegularAdmin">Regular Admin</option>
                            <option value="ComplianceAdmin">Compliance Admin</option>
                        </select>
                        </td> 
                        <td className="td3">
                        {this.state.isSubscribed=="false" && 
                            <span className={(this.state.revokeLable==="Reinstate")?this.state.displayClass:''}>
                            Edit Profile <input type="checkbox" onChange={this._onCheck} />
                            </span>
                        }
                        </td>
                    </tr>
                    <tr>
                        <td className="bold tra-td1">BRM Role Type:</td>
                        <td className="td2"> 
                        <select className="select-tra" value={this.state.BRMRoleType} disabled={this.state.disabled} onChange={this.brmRoleTypeChange}>                        
                            <option value="none">None</option>
                            <option value="BRMView">BRMView</option>
                            <option value="BRMAdmin">BRMAdmin</option>
                            <option value="BRMView">BRMManager</option>
                        </select></td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">First Name:</td>
                        <td className="td2">
                           <input type="text" disabled className="enroll trProfileInput" value={this.state.firstname} readOnly={this.state.readOnly}/> 
                        </td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">Last Name:</td>
                        <td className="td2">
                        <input type="text" disabled className="enroll trProfileInput" value={this.state.lastname} readOnly={this.state.readOnly}/> 
                        </td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">Employee Number:</td>
                        <td className="td2">
                        <input type="text" disabled className="enroll trProfileInput" value={this.state.employeeNumber} readOnly={this.state.readOnly}/>                                                 
                        </td> 
                    </tr>
                    <tr>
                        <td className="bold tra-td1">E-mail:</td>
                        <td className="td2">
                        <input type="text" disabled className="enroll trProfileInput" value={this.state.email} readOnly={this.state.readOnly} />                         
                        </td> 
                    </tr>
                    <tr>
                        <td>

                        </td>
                    </tr>
                    </tbody>
                    </table>
                    {this.state.isSubscribed=="false" && 
                        <span>
                        <button onClick={this.onRevoke}>{this.state.revokeLable}</button>
                        <button onClick={this.onClear} className={this.state.displayClass}>Clear</button>
                        <button onClick={this.onUpdate} className={this.state.displayClass}>Update</button>
                        </span>              
                    }
                    {this.state.isSubscribed=="true" && 
                        <center>
                            <button onClick={this.onSubscribe}>Subscribe</button>
                        </center>
                    }
            </div>
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
                {message}
                <form>
                 <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button onClick={this.onMainSearch}>Main Search</button>
                                    {/* <button onClick={this.onResetPassword}>Reset Password</button> */}
                                    {/* <button onClick={this.deleteUserID}>Delete User ID</button> */}
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}

const mapStateToProps = (state) => {
	return {
		traFormData: state.traFormData
	}	
};

// dispatching into store 
const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadTraFormData
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
    )(TraProfile);