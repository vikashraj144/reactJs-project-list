import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";

class EnrollTra extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentServicingType: 'AAH',
            userId:'',
            fullName:'',
            policyNumber:'',
            emailId:'',
            mobileNumber:'',
            dob:'',
            class:'',
            errorMsg:'',
            pass:'',
        }
        this.emailHandleChange = this.emailHandleChange.bind(this);
        this.userIDhandleChange = this.userIDhandleChange.bind(this);
        this.fullNamehandleChange = this.fullNamehandleChange.bind(this);
        this.policyNumberhandleChange = this.policyNumberhandleChange.bind(this);
        this.mobileNumberhandleChange = this.mobileNumberhandleChange.bind(this);
        
    }

    
  emailHandleChange = (event) => {
    var emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let inputVal = event.target.value;
    if(inputVal.length>=5 && inputVal.length <= 254 && emailRegExp.test(inputVal)){
      this.setState({emailId: event.target.value,class:'',errorMsg:''});
    }else if(inputVal.length === 0){
      this.setState({class:'error',errorMsg: "Please enter Email ID or SSN",pass:''});
    }else{
      this.setState({class:'error',errorMsg: "Invalid Email ID",pass:''});
    }    
  }


    fullNamehandleChange = (event) => {
        if(event.target.value.length != 0){
          this.setState({fullName: event.target.value,class:'',errorMsg:''});
        }else{
          this.setState({class:'error',errorMsg: "Last Name is can not be blank"});
        }
          console.log(event.target.value);
    }  

    mobileNumberhandleChange = (event) => {
      if(event.target.value.length === 0){
          this.setState({class:'error',errorMsg: "Mobile Number can not be blank"});
      }else if(event.target.value.length < 10){
        this.setState({class:'error',errorMsg: "Mobile Number is not valid"});
      } else{
            this.setState({mobileNumber: event.target.value,class:'',errorMsg:''});
        }    
  }  

    policyNumberhandleChange = (event) => {
        if(event.target.value.length === 0){
            this.setState({class:'error',errorMsg: "Policy Number can not be blank"});
          }else{
              this.setState({policyNumber: event.target.value,class:'',errorMsg:''});
          }    
    }  

  userIDhandleChange = (event) => {
    let RegExpression = /^[(!@$-%^)a-zA-Z0-9 \s]*$/;  
    let inputUserIDVal = event.target.value.replace(/\s/g, "");
    if(inputUserIDVal.length>=5 && inputUserIDVal.length <= 20 && RegExpression.test(inputUserIDVal)){
      this.setState({userId: event.target.value,class:'',errorMsg:''});
    }else if(inputUserIDVal.length === 0){
      this.setState({class:'error',errorMsg: "Enter User ID"});
    }else{
      this.setState({class:'error',errorMsg: "The User ID must contain 5 to 20 characters, at least one letter, no spaces and not the following characters ' & < > / \ * ? Blank"});
    }   
    console.log(inputUserIDVal);
     
  }

    onMainSearch = () => {
        this.props.history.push('/csi/aah');
    }
    
    onReset = (event) => {
        document.getElementById("enrollForm").reset();
        event.preventDefault();
    }

    onEnroll = (event) =>{
        if(this.state.fullName.length === 0 || this.state.userId.length === 0 || this.state.policyNumber.length=== 0 || this.state.emailId.length ===0 || this.state.mobileNumber.length ===0){
            this.setState({class:'error',errorMsg: "Please enter details"});
            event.preventDefault();    
          }else{
            this.setState({class:'error',errorMsg:"Enroll operation successfull - Temporary password is: ",pass:'!3456$%#'});
            // this.props.history.push('/csi/aah/profile')
            document.getElementById("enrollForm").reset();
            event.preventDefault();    
          }
    }

    render(){
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Enroll New Customer AAH</h1>
        	</div> 
        <div className="bauForms">
           {/* <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p> */}
            <form className="enroll-new" id="enrollForm">
           <MessageBar errorMsg={this.state.errorMsg} pass={this.state.pass} className={this.state.class}/>
<br></br>
                        
                        <select className="select-tra-enroll">
                        <option value="">TR Role Type</option>
                            <option value="SuperAdmin">Super Admin</option>
                            <option value="RegularAdmin">Regular Admin</option>
                            <option value="ComplianceAdmin">Compliance Admin</option>
                        </select><br></br>
                        <select className="select-tra-enroll">                        
                            <option value="">BRM (Business Rules Manager) Role Type</option>
                            <option value="brm1">BRM View</option>
                            <option value="brm2">BRM View1</option>
                            <option value="brm3">BRM View2</option>
                        </select>
               <input type="text" placeholder="User ID" value={this.state.value} className="enroll" onChange={this.userIDhandleChange}/> 
               <input type="text" placeholder="First Name" value={this.state.value} className="enroll" onChange={this.fullNamehandleChange}/> 
               <input type="text" placeholder="Last Name" value={this.state.value} className="enroll" onChange={this.fullNamehandleChange}/> 
               <input type="text" placeholder="Employee Number" value={this.state.value} className="enroll" onChange={this.policyNumberhandleChange}/> 
               {/* <input type="date" value={this.state.value} className="enroll"/>  */}
               <input type="text" placeholder="Email ID" value={this.state.value} className="enroll" onChange={this.emailHandleChange} /> 
               {/* <input type="number" placeholder="Mobile Phone Number" value={this.state.value} className="enroll" onChange={this.mobileNumberhandleChange}/>  */}
               <div className="bttns">
                  <button onClick={this.onEnroll}>Enroll</button>
                  <button onClick={this.onReset}>Clear</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
                  {/* <span className="resetbtn" onClick={this.onReset}>Clear</span>  */}
               </div>   
            </form>

        </div>
        </div>
        )
    }
}

export default EnrollTra;