import React, { Component } from "react";
import { Link } from "react-router-dom";
//Redux imports
import { loadAAHFormData } from "./../../actions/dataAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Utils from "../../utils/Utils";
// import '../../resources/css/loader.css';

class AahUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentServicingType: 'AAH',
            firstUserId: '',
            secondUserId: '',
            firstUserLastName: '',
            secondUserLastName: ''
        }
    }

    componentDidMount = () => {

        Utils.pLoader();
        const { aahFormData } = this.props;
        if(aahFormData.length !== 0) {
            this.setState({ firstUserId: this.props.aahFormData.Users[0].identityProfile[0].value,
                secondUserId: this.props.aahFormData.Users[1].identityProfile[0].value,
                firstUserLastName: this.props.aahFormData.Users[0].businessProfile.lastName,
                secondUserLastName: this.props.aahFormData.Users[1].businessProfile.lastName,
                firstUserStatus: this.props.aahFormData.Users[0].identityProfile[2].value,
                secondUserStatus: this.props.aahFormData.Users[1].identityProfile[2].value,
                firstUserGuid: this.props.aahFormData.Users[0].identityProfile[1].value,
                secondUserGuid: this.props.aahFormData.Users[1].identityProfile[1].value });
            Utils.closeLoader();
        } else {
            this.props.history.push('/csi/aah');
        }
    }

    onMainSearch = (event) => {
        this.props.history.push('/csi/aah');
    }
    
    handleFirstUserIdClick = () => {

        let aahFormData = Object.assign({},this.props.aahFormData);
        aahFormData.selectedGuid = this.state.firstUserGuid;
        this.props.loadAAHFormData(aahFormData);
        this.props.history.push('/csi/aah/profile');
    }

    handleSecondUserIdClick = () => {

        let aahFormData = Object.assign({},this.props.aahFormData);
        aahFormData.selectedGuid = this.state.secondUserGuid;
        this.props.loadAAHFormData(aahFormData);
        this.props.history.push('/csi/aah/profile');
    }
    
    render() {
        return (
            <div>
                <div className="titleBox">
                    <h1><span>&raquo;</span>Select User ID</h1>
                </div> 
                <div className="profileForms">
                            {/* 1st tab start */}
                            <div className="divTable">
                                <div className="divTableBody">
                                    <div className="divTableRow">
                                        <div className="divTableCell">User ID</div>
                                        <div className="divTableCell">Last Name</div>
                                        <div className="divTableCell">SSO Status</div>
                                    </div>
                                    <div className="divTableRow">
                                        <div className="divTableCell">
                                            <a onClick={this.handleFirstUserIdClick}
                                                style={{textDecoration: 'underline'}}>{this.state.firstUserId}</a>
                                        </div>
                                        <div className="divTableCell">{this.state.firstUserLastName}</div>
                                        <div className="divTableCell">{this.state.firstUserStatus}</div>
                                    </div> 
                                    <div className="divTableRow">
                                        <div className="divTableCell">
                                            <a onClick={this.handleSecondUserIdClick}
                                                style={{textDecoration: 'underline'}}>{this.state.secondUserId}</a>
                                        </div>
                                        <div className="divTableCell">{this.state.secondUserLastName}</div>
                                        <div className="divTableCell">{this.state.secondUserStatus}</div>
                                    </div> 
                                </div>
                            </div>
                            {/* 1st tab end */}
                    </div>
                    <div className="center">
                            <button onClick={this.onMainSearch}>Main Search</button>
                    </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
	return {
        aahFormData: state.aahFormData
    }
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadAAHFormData
}, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AahUser);