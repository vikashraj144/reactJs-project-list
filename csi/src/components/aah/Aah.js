import React, { Component } from "react";
import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
import * as Constants from '../../utils/Constants';
import ConfigService from '../../api/ConfigService';
import Utils from '../../utils/Utils';
// import '../../resources/css/loader.css';
//Redux imports
import { loadAAHFormData } from "./../../actions/dataAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as jsonData from './aah_searchResponse.json';
import * as jsonMultipleData from './aah_searchResponse_multipleGuid.json';

class Aah extends Component {

constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'AAH',
        userId:'',
        policyNumber:'',
        class:'error',
        errorMsg:'',
        disableUserId: false,
        disablePolicyNumber: false,
        validated:false,
    }
    
    this.userIDhandleChange = this.userIDhandleChange.bind(this);
    this.policyNumberHandleChange = this.policyNumberHandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  	componentDidMount = () => {

		Utils.pLoader();
  		//ConfigService.getSession();
  		//ConfigService.getToken();
		Utils.closeLoader();
  	}

  policyNumberHandleChange = (event) => {
    let RegExpression = /^[a-zA-Z0-9 \s]*$/;  
    let policyNumber = event.target.value;

    let disableUserIdVal = (event.target.value.length)?true:false;
    this.setState({ validated:false,policyNumber: policyNumber,disableUserId:disableUserIdVal });

    if(policyNumber.length !== 10 && RegExpression.test(policyNumber)){
      if(policyNumber.replace(/[^0-9]/g,"").length !==8 ){
      this.setState({class:'error',errorMsg: "Policy Number should be at least 8 numeric and 2 alphabetic character"});
      }else if(policyNumber.replace(/[^a-zA-Z]/g,"").length !==2){
        this.setState({class:'error',errorMsg: "Policy Number must include 2 letters"});
      }
    }else if(event.target.value.length>0){
      this.setState({validated:true,class:'',errorMsg:''});
    }
    
  }

  userIDhandleChange = (event) => {
    let inputVal = event.target.value;
    
    let disablePolicyVal = (inputVal.length)?true:false;
    this.setState({ validated:false,userId: inputVal,disablePolicyNumber:disablePolicyVal });
    let valid = Utils.regExAlphaNumeric(inputVal);
		if(valid) {
      if(inputVal.length>=5 && inputVal.length <= 20){
        this.setState({userId: inputVal,validated:true,class:'',errorMsg:''});
        if(!Utils.regExOneAlphabetsAtleastOneNumber(inputVal)){
          this.setState({class: 'error',errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR,validated:false});
        }
      }else if(inputVal.length === 0){
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION});
      }else{
        this.setState({ class: 'error',errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
      }    
    }
  }

  onSearch = (event) => {

    Utils.pLoader();
    event.preventDefault();    
    if(this.state.userId.length === 0 && this.state.policyNumber.length ===0){
      this.setState({class:'error',errorMsg: "Enter either policy Number or User ID ."});        
    }else if(this.state.validated) {
        if(this.state.policyNumber.length > 0) {
            
            //AAH Search service using Policy Number
            fetch(Constants.e1BaseURL + '/csi/v1/aah/client/profile',
            {
				method: `POST`,
				//credentials: `include`,//Comment at local
                headers: {
                    'Content-Type': 'application/json',
                    'apiToken': window.sessionStorage.getItem("apiToken"),
                    'SMSESSION': window.sessionStorage.getItem("SMSESSION"),//Comment at E1
                    'selectedBU': 'AAH'
                },
                body: JSON.stringify({ 'policyNumber': this.state.policyNumber })
            }).then((res) => {

				if(res.status === 200) {

                    window.sessionStorage.setItem("apiToken", res.headers.get('apitoken'));
					res.json().then((response) => {
						/* console.log("jsonData-----", jsonData);
						console.log("jsonMultipleData---", jsonMultipleData);
						response = jsonData; */
						if((Object.keys(response)[0]).toLowerCase() === 'users') {
							//Multiple Users
							response.selectedGuid = '';
							this.props.loadAAHFormData(response);
							Utils.closeLoader();
							  this.props.history.push('/csi/aah-user-list');
						} else {
							  //Single User
							this.props.loadAAHFormData(response);
							Utils.closeLoader();
							this.props.history.push('/csi/aah/profile'/* ,{userId: jsonData.Identity_Profile[0][0].value} */)
						}
					})
				} else {
					res.json().then((response) => {
                        console.log("response due to----", response);
                        Utils.closeLoader();
					})
				}
            }).catch((error) => {
                console.log("AAH error----", error);
                Utils.closeLoader();
            })
      } else {

        //AAH Search service using User Id
        fetch(Constants.e1BaseURL + '/csi/v1/aah/client/profile',
        {
			method: `POST`,
			credentials: `include`,
            headers: {
                'Content-Type': 'application/json',
                'apiToken': window.sessionStorage.getItem("apiToken"),
                /* 'SMSESSION': window.sessionStorage.getItem("SMSESSION"), */
                'selectedBU': 'AAH'
            },
            body: JSON.stringify({ 'userId': this.state.userId })
          }).then((res) => {

            if(res.status === 200) {
                res.json().then((response) => {
                    //let aahData = response;
                    this.props.loadAAHFormData(response);
                    Utils.closeLoader();
                    this.props.history.push('/csi/aah/profile');
                  })
            } else {
                res.json().then((response) => {
                    console.log("Search error ---", response);
                    Utils.closeLoader();
                })
            }
          })
          .catch((error) => {
              console.log("AAH error----", error);
              Utils.closeLoader();
          })
            
      }
    }
    
  }

  onReset = (event) => {
    // document.getElementById("userForm").reset();
    this.setState({errorMsg:'',disableUserId:false,disablePolicyNumber:false,policyNumber:'',userId:''})
    event.preventDefault();    

  }
  enrollNew = () => {
    this.props.history.push('/csi/aah-enroll-new')
  }

  render() {
    let message;
    if(this.state.errorMsg){
      message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
      if(this.props.location.state){
        this.props.location.state.msg = "";
      }
    }else if(this.props.location.state){
      message = <MessageBar errorMsg={this.props.location.state.msg} className="success"/>
    }

    return (
      <div>
        <HeaderTab />
       <div className="bauSelect align-center">
          <strong>&raquo; </strong><b>Current Servicing Type</b> <strong>-</strong> {this.state.currentServicingType}            
       </div>
        <div className="bauForms">
           <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p>
           {message}
           <br></br>
              <form id='userForm'>
               <input type="text" placeholder="Policy Number" disabled={this.state.disablePolicyNumber} value={this.state.policyNumber} onChange={this.policyNumberHandleChange} /> 
               <span className='orColor'>Or</span>
               <input type="text" placeholder="User ID" disabled={this.state.disableUserId} value={this.state.userId} onChange={this.userIDhandleChange} /> 
               <div className="bttns">
                  <button onClick={this.onSearch}>Search</button>
                  <button onClick={this.enrollNew}>Enroll New</button>
                  <button onClick={this.onReset}>Clear</button>
                  {/* <span className="resetbtn" onClick={this.onReset}>Clear</span>  */}
               </div>   
            </form>

        </div>
        
      </div>
    );
  }
}
 
const mapStateToProps = (state) => {
	return {
		aahFormData: state.aahFormData
	}	
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadAAHFormData
}, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Aah);