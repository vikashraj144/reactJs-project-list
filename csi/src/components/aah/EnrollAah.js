import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
import * as Constants from '../../utils/Constants';
import Utils from '../../utils/Utils';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import '../../resources/css/customChanges.css';

class EnrollAah extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentServicingType: 'AAH',
            userId:'',
            lastName:'',
            policyNumber:'',
            dob: '',
            birthDate: '',
            emailId:'',
            mobileNumber:'',
            class:'',
            errorMsg:'',
            pass:'',
        }
        this.emailHandleChange = this.emailHandleChange.bind(this);
        this.userIDhandleChange = this.userIDhandleChange.bind(this);
        // this.fullNamehandleChange = this.fullNamehandleChange.bind(this);
        this.lastNamehandleChange = this.lastNamehandleChange.bind(this);
        this.policyNumberhandleChange = this.policyNumberhandleChange.bind(this);
        this.mobileNumberhandleChange = this.mobileNumberhandleChange.bind(this);
        
    }

    
  emailHandleChange = (event) => {
    // var emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let inputVal = event.target.value;
    this.setState({emailId:event.target.value});
    let emailRegExp = Utils.emailRegExp(inputVal); 
    if(inputVal.length>=5 && inputVal.length <= 254 && emailRegExp){
      this.setState({emailId: event.target.value,class:'',errorMsg:''});
    }else if(inputVal.length === 0){
      this.setState({class:'error',errorMsg: "Please enter Email ID or SSN",pass:''});
    }else{
      this.setState({class:'error',errorMsg: "Invalid Email ID",pass:''});
    }    
  }


    // fullNamehandleChange = (event) => {
    //     if(event.target.value.length !== 0){
    //       this.setState({fullName: event.target.value,class:'',errorMsg:''});
    //     }else{
    //       this.setState({class:'error',errorMsg: "Last Name is can not be blank"});
    //     }
    //       console.log(event.target.value);
    // }  

    lastNamehandleChange = (event) => {
      let valid = Utils.regExAlphabetsOnly(event.target.value);
      if(valid){
        if(event.target.value.length >= 1){
          this.setState({lastName: event.target.value,class:'',errorMsg:''});
        }
        else{
          this.setState({class:'error',errorMsg: "Name can not be empty"});
        }
      }else{
        this.setState({class:'error',errorMsg: "Name must contain only letters"});
      }
    }

    mobileNumberhandleChange = (event) => {
      if(event.target.value.length === 0){
          this.setState({class:'error',errorMsg: "Mobile Number can not be blank"});
      }else if(event.target.value.length !== 10){
        this.setState({class:'error',errorMsg: "Mobile Number is not valid"});
      } else{
            this.setState({mobileNumber: event.target.value,class:'',errorMsg:''});
        }    
    }  

    // policyNumberhandleChange = (event) => {
    //     if(event.target.value.length === 0){
    //         this.setState({class:'error',errorMsg: "Policy Number can not be blank"});
    //       }else{
    //           this.setState({policyNumber: event.target.value,class:'',errorMsg:''});
    //       }    
    // }  
    policyNumberhandleChange = (event) => {
      let RegExpression = /^[a-zA-Z0-9 \s]*$/;  
      let policyNumber = event.target.value;
  
      let disableUserIdVal = (event.target.value.length)?true:false;
      this.setState({ validated:false,policyNumber: policyNumber,disableUserId:disableUserIdVal });
  
      if(policyNumber.length !== 10 && RegExpression.test(policyNumber)){
        if(policyNumber.replace(/[^0-9]/g,"").length !==8 ){
        this.setState({class:'error',errorMsg: "Policy Number should be at least 8 numeric and 2 alphabetic character"});
        }else if(policyNumber.replace(/[^a-zA-Z]/g,"").length !==2){
          this.setState({class:'error',errorMsg: "Policy Number must include 2 letters"});
        }
      }else if(event.target.value.length>0){
        this.setState({validated:true,class:'',errorMsg:''});
      }
      
    } 

    /**
	 * Function to handle User ID input
	 */
	userIDhandleChange = (event) => {
		// Removing spaces
    let userIDValue = Utils.trimSpaces(event.target.value);
    
    let disableGUIDVal = (event.target.value.length)?true:false;
		this.setState({ userId: userIDValue,validated:false,disableGUID:disableGUIDVal });
        let valid = Utils.regExAlphaNumeric(userIDValue);
		if(valid) {
			if (userIDValue.length >= 5 && userIDValue.length <= 20) {
                this.setState({ validated: true, class:'', errorMsg:''});
                if(!Utils.regExOneAlphabetsAtleastOneNumber(userIDValue)){
				this.setState({class: 'error', validated: false, errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR });
                }                
			} else if (userIDValue.length === 0) {
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION});
			} else {
				this.setState({ class: 'error', validated: false, 
					errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
			}
        }
        
  }

    onMainSearch = () => {
        this.props.history.push('/csi/aah');
    }
    
    onReset = (event) => {
      event.preventDefault();
      document.getElementById("enrollForm").reset();
      this.setState({})
    }

    /**
     * Function on Enroll new user
     */
    onEnroll = (event) => {
      
      event.preventDefault();    
        if(this.state.lastName.length === 0 || this.state.userId.length === 0 || this.state.policyNumber.length=== 0 || this.state.emailId.length ===0 || this.state.mobileNumber.length ===0){
            this.setState({class:'error',errorMsg: "Please enter details"});
        } else {
            fetch(Constants.e1BaseURL + '/csi/v1/aah/client/profile',
            {
				method: `POST`,
				//credentials: `include`,// To comment in local
                headers: {
                    'Content-Type': 'application/json',
                    'apiToken': window.sessionStorage.getItem("apiToken"),
                    'SMSESSION': window.sessionStorage.getItem("SMSESSION"),
                    'selectedBU': 'AAH'
                },
                body: JSON.stringify({
                  
                    "userId": this.state.userId,
                    "lastName": this.state.lastName,
                    "policyNumber": this.state.policyNumber,
                    "dob": this.state.dob,
                    "clientId": this.state.userId,
                    "emailAddress": this.state.emailId,
                    "mobileNumber": this.state.mobileNumber,
                    "subscribedServicesCd" : "233" })
            }).then((res) => {

                console.log("res----", res);
                res.json().then((response) => {
                    let newTempPwd = (response.identityProfile[2].value).split('').join(' ');
                    //let data = {msg :"Enroll operation successfull - Temporary password is: ",pass:'!3456$%#'}
                    this.props.history.push('/csi/aah/profile',{msg :"Enroll operation successfull - Temporary password is: ",pass: newTempPwd});
                    Utils.closeLoader();
                })
            }).catch((error) => {
                console.log("AAH error----", error);
                Utils.closeLoader();
            })
            
            
            // document.getElementById("enrollForm").reset();
            // event.preventDefault();    
          }
    }

    /**
     * Function to handle date change
     */
    handleDateChange = (date) => {

        if(typeof date === 'object') {
            this.setState({
                dob: moment(date).format('MM/DD/YYYY'), birthDate: date
              });
        } else {
            this.setState({ dob: '' });
        }
    }

    render() {
        return( 
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>AAH Enroll New Customer</h1>
        	</div> 
        <div className="bauForms">
        {/* <div className="titleBox1">
                <h3>
                  Enroll New Customer
                  </h3>
        	</div>  */}
           {/* <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p> */}
            <form className="enroll-new" id="enrollForm">
            {/* <br></br> */}

           <MessageBar errorMsg={this.state.errorMsg} pass={this.state.pass} className={this.state.class}/>
            <br></br>
            <input type="text" placeholder="User ID" value={this.state.userId} className="enroll" onChange={this.userIDhandleChange}/> 
            <input type="text" placeholder="Last Name" value={this.state.lastName} className="enroll" onChange={this.lastNamehandleChange}/> 
            <input type="text" placeholder="Policy Number" value={this.state.policyNumber} className="enroll" onChange={this.policyNumberhandleChange}/> 
            {/* <input type="date" placeholder="Date of Birth (mm/dd/yyyy)" onChange={this.handleDateChange} className="enroll"/>  */}
            <DatePicker
                onChange={this.handleDateChange}
                peekNextMonth
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                placeholderText="DOB (MM/DD/YYYY)"
                selected={this.state.birthDate}
            />
            <input type="text" placeholder="Email ID" value={this.state.emailId} className="enroll" onChange={this.emailHandleChange} /> 
            <input type="text" placeholder="Mobile Phone Number (optional)" value={this.state.mobileNumber} className="enroll" onChange={this.mobileNumberhandleChange}/> 

               <div className="bttns">
                  <button onClick={this.onEnroll}>Enroll</button>
                  <button onClick={this.onReset}>Clear</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
                  {/* <span className="resetbtn" onClick={this.onReset}>Clear</span>  */}
               </div>   
            </form>

        </div>
        </div>
        )
    }
}

export default EnrollAah;