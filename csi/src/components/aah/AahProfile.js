import React,{ Component } from "react";
import MessageBar from "../shaired/MessageBar";
// import InputMessageBar from "../shaired/InputMessageBar";
import * as Constants from '../../utils/Constants';
import Utils from '../../utils/Utils';
// import '../../resources/css/loader.css';
//Redux imports
import { loadAAHFormData } from "./../../actions/dataAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class AahProfile extends Component {

    constructor(props){
        super(props);
        this.state = {
            title:' AAH Customer Information ',
            class:'',
            errorMsg:'',
            pass:'',
            isSubscribe:'Unsubscribe',
            display:'',
            policyNumber:'BX07461150',
            firstUser: {
                userId : '', userGuid : '', lastLoginDate : '', userStatus : '', pwdStatus : '', 
                isSubscribed : '', subscribedServices : '', dateOfBirth : '', firstName : '', 
                ssoGuid : '', lastName : '', phoneNumber : '', emailAddress : '', policyNumber: '' },
            secondUser: {
                userId : '', userGuid : '', lastLoginDate : '', userStatus : '', pwdStatus : '', 
                isSubscribed : '', subscribedServices : '', dateOfBirth : '', firstName : '', 
                ssoGuid : '', lastName : '', phoneNumber : '', emailAddress : '', policyNumber: '' },
        };
    }

    /**
     * Function added to get data on render - test method Sumant
     */
    componentDidUpdate = (prevProps) => {

        if(this.props.userId !== prevProps.userId) {
            console.log("hellozzzzzzzzzz");
        }
    }

    componentDidMount = () => {

        const { aahFormData } = this.props;
        if(aahFormData.length !== 0) {
            
            //Check for Multiple User Id's
            if((typeof aahFormData.Users) != 'undefined') {

                if(aahFormData.Users.length === 3) {
                    //3 User Id's
                } else if(aahFormData.Users.length === 2) {
                    //Multiple User id
                    let firstUser, secondUser;
                    if(aahFormData.Users[0].identityProfile[1].value === aahFormData.selectedGuid) {
                        firstUser = this.setFirstUserValues(aahFormData.Users[0]);
                        secondUser = this.setSecondUserValues(aahFormData.Users[1]);
                    } else {
                        firstUser = this.setFirstUserValues(aahFormData.Users[1]);
                        secondUser = this.setSecondUserValues(aahFormData.Users[0]);
                    }
                    this.setState({ firstUser, secondUser });
                }
            } else {
                //Single User id
                let firstUser = this.setSingleUserValues(aahFormData);
                this.setState({ firstUser });
            }
        } else {
            this.props.history.push('/csi/aah');
        }
    }

    /**
     * Function to set data to First User in case of Multiple user Id's
     */
    setFirstUserValues = (data) => {

        let firstUser = this.state.firstUser;
        firstUser.userId = data.identityProfile[0].value;
        firstUser.userGuid = data.identityProfile[1].value;
        firstUser.userStatus = data.identityProfile[2].value;
        firstUser.lastName = data.businessProfile.lastName;
        return firstUser;
    }

    /**
     * Function to set data to Second User in case of Multiple user Id's
     */
    setSecondUserValues = (data) => {

        let secondUser = this.state.secondUser;
        secondUser.userId = data.identityProfile[0].value;
        secondUser.userGuid = data.identityProfile[1].value;
        secondUser.userStatus = data.identityProfile[2].value;
        secondUser.lastName = data.businessProfile.lastName;
        return secondUser;
    }

    /**
     * Function to set data to FirstUser in case of Single User Id
     */
    setSingleUserValues = (data) => {

        let firstUser = this.state.firstUser;
        firstUser.userId = data.identityProfile[0].value;
        firstUser.userGuid = data.identityProfile[1].value;
        firstUser.lastLoginDate = data.identityProfile[2].value;
        firstUser.userStatus = data.identityProfile[3].value;
        firstUser.pwdStatus = data.identityProfile[4].value;
        firstUser.isSubscribed = data.identityProfile[5].value;
        firstUser.subscribedServices = data.identityProfile[6].value;
        firstUser.dateOfBirth = data.businessProfile[0].dob;
        firstUser.firstName = data.businessProfile[0].FirstName;
        firstUser.ssoGuid = data.businessProfile[0].SsoGuid;
        firstUser.lastName = data.businessProfile[0].lastName;
        firstUser.phoneNumber = data.businessProfile[0].mobilenumber;
        firstUser.emailAddress = data.businessProfile[0].emailAddress;
        firstUser.policyNumber = data.businessProfile[0].policynumber;
        return firstUser;
    }

    onMainSearch = () => {

        this.props.loadAAHFormData([]);
        this.props.history.push('/csi/aah');
    }
    
    
    subscribe = () => {
        console.log('id',this.props.match );

        if(this.state.isSubscribe ==='Unsubscribe') {
            // this.setState({class:'error',errorMsg: "Are you sure you want to Unsubscribe the user ?"});
            if (window.confirm("Are you sure you want to unsubscribe the user ?")) {
                this.setState({isSubscribe:'Subscribe'});
                this.setState({isSubscribe:'',display:'display-non'});
                this.props.history.push('/csi/aah',{msg:'Unsubscribed operation Successful'});
            }

        }
    }
    
    toggleSubscribe = () => {
        this.setState({class:'',errorMsg: ""});
    }

    policyNumberChange = (event) => {
        
        this.setState({policyNumber:event.target.value});
        
    }

    /**
     * Function on Reset Password
     */
    onResetPassword = (event) => {

        if (window.confirm("Are you sure you want to reset password?")) {
            
            Utils.pLoader();
            //Method to call on Reset Password
            fetch(Constants.e1BaseURL + '/csi/v1/resetClientPwd', 
            {
                method: `POST`,
                //credentials: `include`,//To comment in local
                headers: {
                    'Content-Type': 'application/json',
                    'apiToken': window.sessionStorage.getItem("apiToken"),
                    'SMSESSION': window.sessionStorage.getItem("SMSESSION"), //To remove in E1
                    'selectedBU': 'AAH'
                },
                body: JSON.stringify({ "userId": this.state.firstUser.userId })
            }).then((res) => {
                if(res.status === 200) {
                    res.json().then((response) => {
                        let newTempPwd = (response.identityProfile[2].value).split('').join(' ');
                        this.setState({ class: 'error', pass: newTempPwd,
                            errorMsg: "Reset Password Successful - New password is: "});
                        Utils.closeLoader();
                    })
                    
                } else {
                    res.json().then((response) => {
                        console.log("Aah reset password Error----", response);
                        Utils.closeLoader();
                    })
                    
                }
            }).catch((error) => {
                console.log("AAH Reset password error----", error);
                Utils.closeLoader();
            })
        } else {
            this.setState({class:"",errorMsg:"",pass:""});
        }
        event.preventDefault();    
        
    }
    
    onEnroll = () => {
        this.props.history.push('/csi/aah-enroll-new');
    }

    render(){
        let message;
        if(this.state.pass!==''){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }
        if(this.props.location.state){
            if(this.props.location.state.msg){
                message = <MessageBar errorMsg={this.props.location.state.msg} pass={this.props.location.state.pass} className="error"/>
            }
        }

        return(
            <div>
                <div className="titleBox">
                    <h1><span>&raquo;</span>{this.state.title}</h1>
                </div>
                <div className="profileForms">
                    <table>
                        <tbody>
                            <tr className="profile-head-color">
                                <th>Security Identity Data</th>
                                <th></th>
                                <td>Enrolled Services</td> 
                            </tr>
                            <tr>
                                <td className="bold td1">User ID:</td>
                                <td className="td2">{this.state.firstUser.userId}</td> 
                                <td className="td3">{this.state.firstUser.subscribedServices}</td> 
                            </tr>
                            <tr>
                                <td className="bold td1">Status:</td>
                                <td className="td2">{this.state.firstUser.userStatus}</td> 
                            </tr>
                            <tr>
                                <td className="bold td1">GUID:</td>
                                <td className="td2">{this.state.firstUser.userGuid}</td> 
                            </tr>
                            <tr>
                                <td className="bold td1">Last Login:</td>
                                <td className="td2">{this.state.firstUser.lastLoginDate}</td> 
                            </tr>
                            <tr>
                                <td className="bold td1">Password Status:</td>
                                <td className="td2">{this.state.firstUser.pwdStatus}</td> 
                            </tr>
                        </tbody>
                    </table>
                </div>
                {/* 2nd tab start */}
                <div className="profileForms">
                    <div className="profile-head-color">
                        <span className="profile-head-left">Profile Information</span>
                    </div>
                    {/* {inputMsg} */}
                    <table>
                        <tbody>
                            <tr>
                                <th>Policy Details</th>
                                <th></th>
                                <th>User Profile</th>
                                <th>First User</th>
                                <th>Second User</th>
                            </tr>
                            <tr>
                                <td>Policy Number</td>
                                <td>
                                    <select className="select-aah-profile" onChange={this.policyNumberChange}>
                                        <option value="BX07461150">BX07461150-Registered</option>
                                        <option value="BX07461151">BX07461151-Registered</option>
                                        <option value="BX07461152">BX07461152-Registered</option>
                                    </select>    
                                </td> 
                                <td>User ID	</td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.firstUser.userId} onChange={this.namehandleChange} readOnly/></td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.secondUser.userId} onChange={this.namehandleChange} readOnly/></td> 
                            </tr>
                            <tr>
                                <td>Policy Selected</td>
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.policyNumber} onChange={this.namehandleChange} readOnly/></td>       
                                <td>Last Name</td>
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.firstUser.lastName} onChange={this.namehandleChange} readOnly/></td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.secondUser.lastName} onChange={this.namehandleChange} readOnly/></td> 
                            </tr>
                            <tr>
                                <td>Policy Status</td>
                                <td><input type="text" className="enroll aah-profile-input" value="Registered" onChange={this.namehandleChange} readOnly/></td>       
                                <td>Date of Birth</td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.firstUser.dateOfBirth} onChange={this.namehandleChange} readOnly/></td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.secondUser.dateOfBirth} onChange={this.namehandleChange}/></td> 
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>       
                                <td>Email Address</td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.firstUser.emailAddress} onChange={this.namehandleChange} readOnly/></td> 
                                <td><input type="text" className="enroll aah-profile-input"  value={this.state.secondUser.emailAddress} onChange={this.namehandleChange} readOnly/></td> 
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>       
                                <td>Mobile Number</td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.firstUser.phoneNumber} onChange={this.namehandleChange} readOnly/></td> 
                                <td><input type="text" className="enroll aah-profile-input" value={this.state.secondUser.phoneNumber} onChange={this.namehandleChange} readOnly/></td> 
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                <button className={this.state.display} onClick={this.subscribe}>Unsubscribe</button>
                                </td>
                                <td></td>       
                            </tr>
                        </tbody>
                    </table>
                </div>
                {/* 2nd tab end */}
                {/* 3rd tab start */}
                <div className="profileForms">
                    <div className="profile-head-color">
                        <span className="profile-head-left">Security Functions</span>
                    </div>
                {message}
                <form>
                    <div className="divTable">
                        <div className="divTableBody">
                            <div className="divTableRow">
                                <center>
                                        <button onClick={this.onMainSearch}>Main Search</button>
                                        <button onClick={this.onResetPassword}>Reset Password</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        {/* 3rd tab end */}
        </div>
        )
    }
}

const mapStateToProps = (state) => {
	return {
        aahFormData: state.aahFormData
    }
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadAAHFormData
}, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AahProfile);