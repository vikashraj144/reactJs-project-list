import React from "react";
import {NavLink} from 'react-router-dom';

const HeaderTab = () => {

    const links = [
        {name: 'MYFA', url: '/csi', span:'My Accounts R3A'},
        {name: 'CC90', url: '/csi/cc90',span:'cc90'},
        {name: 'AAH', url: '/csi/aah',span:'Auto and Home Insurance'},
        // {name: 'RVS', url: '/rvs',span:'Riversource FP'},
        {name: 'PAP', url: '/csi/pap',span:'Professional Alliance Program'},
        {name: 'SEP', url: '/csi/sep',span:'Seminar and Event Program'},
        {name: 'TRA', url: '/csi/tra',span:'TR Admin'},
        {name: 'IFSC', url: '/csi/ifsc',span:'IFSC TPD All'},
    ];

    let linksComponents = links.map((link, index) => {
        return (
            <li key={index}>
                <NavLink className={'navLink'} activeClassName={'activeNavLink'}
                         to={link.url} exact><span className="menu-top">{link.name}</span>{link.span}</NavLink>
            </li>
        );
    });

    return (
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> Main Search</h1>
        	</div> 
             <ul className="appList">
                {linksComponents}
            </ul>
        </div>
    );
};

export default HeaderTab;