import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
// import InputMessageBar from "../shaired/InputMessageBar";
import * as Constants from '../../utils/Constants';

class cc90Profile extends Component{

    constructor(props){
        super(props);
        this.state = {
            title:' CC90 Customer Information ',
            class:'',
            errorMsg:'',
            isSubscribe:'Subscribe',
            pass:''
        };
    }

    onMainSearch = () => {
        this.props.history.push('/csi/cc90');
    }
    
    enrollNew = () => {
        this.props.history.push('/csi/cc90-enroll-new');
    }
    subscribe = () => {
        if(this.state.isSubscribe ==='Unsubscribe'){
            if (window.confirm("Are you sure you want to unsubscribe the user ?")) { 
                this.setState({isSubscribe:'Subscribe',class:'success',errorMsg: Constants.ERROR_MSGS.UNSUBSCRIBE_SUCCESS});
                // this.setState({isSubscribe:'Subscribe'})
              }
        }else if(this.state.isSubscribe ==='Subscribe'){
            // if (window.confirm("Are you sure you want to subscribe the user ?")) { 
                this.setState({isSubscribe:'Unsubscribe'})
                // this.setState({class:'error',errorMsg: "Are you sure you want to subscribe the user ?"});
            //   }
        }
    }

    toggleSubscribe = () => {
        this.setState({class:'',errorMsg: ""});
    }

    onRestPassword = (event) =>{
        if (window.confirm("Are you sure you want to reset password?")) { 
            this.setState({class:'error',errorMsg:"Reset Password Successful - New password is: ",pass:'! 3 4 5 6 $ % #'});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();    
    }
    
    render(){
        let message,enrollMessage;
        if(this.state.pass.length){
            enrollMessage = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }else
        if(this.state.errorMsg.length){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
        }else if(this.props.location.state){
            if(this.props.location.state.enrollMsg){
                enrollMessage = <MessageBar errorMsg={this.props.location.state.enrollMsg} className="success"/>
            }
        }
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> {this.state.title}</h1>
            </div>    
                <div className="profileForms">                
<table>
    <tbody>
  <tr className="profile-head-color">
    <th>Security Identity Data</th>
    <th></th>
    <td>Enrolled Services</td> 
  </tr>
  <tr>
    <td className="bold td1">User ID:</td>
    <td className="td2">David</td> 
    <td className="td3">SSO Service , RVERSESOURCE , TR Admin Service</td> 
  </tr>
  <tr>
    <td className="bold td1">Status:</td>
    <td className="td2">Active</td> 
  </tr>
  <tr>
    <td className="bold td1">QUID:</td>
    <td className="td2">345678</td> 
  </tr>
  <tr>
    <td className="bold td1">Last Login:</td>
    <td className="td2">01/27/2018</td> 
  </tr>
  <tr>
    <td className="bold td1">Password Status:</td>
    <td className="td2">Temporary and Expired</td> 
  </tr>
  </tbody>
</table>
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            <div className="profile-head-color">
                    <span className="profile-head-left">Service Functions</span>
            </div>
           {message}
            <div className="divTable">

                            <div className="divTableBody">
                                <div className="divTableRow">
                                    <div className="divTableCell service-function">CC90</div>
                                    <div className="divTableCell"><button onClick={this.subscribe}>{this.state.isSubscribe}</button></div>
                                </div>
                            </div>
            </div>
            
           
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
                {enrollMessage}
                <form>

                 <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button onClick={this.onRestPassword}>Reset Password</button>
                                    <button onClick={this.onMainSearch}>Main Search</button>
                                    <button onClick={this.enrollNew}>Enroll New</button>
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}

export default cc90Profile;