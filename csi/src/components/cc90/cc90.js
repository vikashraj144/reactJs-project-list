import React, { Component } from "react";
import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
import Header from "../../Header";
import Utils from '../../utils/Utils';
import * as Constants from '../../utils/Constants';
class cc90 extends Component {

constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'CC90',
        userId:'',
        guid:'',
        class:'error',
        errorMsg:'',
		    validated: false,
        disableUserID: false,
        disableGUID: false
    }
    
    this.userIDhandleChange = this.userIDhandleChange.bind(this);
    this.guidHandleChange = this.guidHandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onReset = this.onReset.bind(this); 
  
  }

	/**
	 * Function to handle GUID change
	 */
	guidHandleChange = (event) => {
    let gUIDVal = Utils.trimSpaces(event.target.value);
    let disableGuidVal = (event.target.value.length)?true:false;    
		this.setState({ guid: gUIDVal,disableUserID:disableGuidVal });
		let valid = Utils.regExAlphaNumericOnly(event.target.value);
      if(valid) {
        if(event.target.value.length === 32) {
          this.setState({ validated: true, class:'', errorMsg:'' });
        } else {
          this.setState({ class: 'error', validated: false,
            errorMsg: Constants.ERROR_MSGS.GUID_LENGTH_ERROR });
        }
      } else {
        this.setState({ class:'error', validated: false, 
          errorMsg: Constants.ERROR_MSGS.GUID_VALIDATION_ERROR });
      }
  	}

    /**
	 * Function to handle User ID input
	 */
	userIDhandleChange = (event) => {
		// Removing spaces
    let userIDValue = Utils.trimSpaces(event.target.value);
    let disableGUIDVal = (event.target.value.length)?true:false;
		this.setState({ userId: userIDValue,disableGUID:disableGUIDVal });
        let valid = Utils.regExAlphaNumeric(userIDValue);
		if(valid) {
			if (userIDValue.length >= 5 && userIDValue.length <= 20) {
                this.setState({ validated: true, class:'', errorMsg:''});
                if(!Utils.regExOneAlphabetsAtleastOneNumber(userIDValue)){
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR, validated: false });
                }                
			} else if (userIDValue.length === 0) {
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION, validated: false });
			} else {
				this.setState({ class: 'error', validated: false, 
					errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
			}
		} else {
			this.setState({ validated: false });
		}
  }

  /**
	 * Function on Click search
	 */
  onSearch = (event) =>{
    if(this.state.userId.length === 0 && this.state.guid.length ===0){
      this.setState({class:'error',errorMsg:Constants.ERROR_MSGS.USER_OR_GUID_VALIDATION});
    }else if(this.state.validated){
      this.props.history.push('/csi/cc90/profile')
    }
    event.preventDefault();    
  }

  
  /**
	 * Function on Reset Password
	 */
  onReset = (event) => {
    event.preventDefault();    
    this.setState({userId:'',errorMsg:'',guid:'',disableGUID:false,disableUserID:false});
  }

  /**
	 * Function on enroll new client
	 */
  enrollNew = () => {
    this.props.history.push('/csi/cc90-enroll-new')
  }

  render() {
    return (
      <div>
        <Header />
        <HeaderTab />
       <div className="bauSelect align-center">
          <strong>&raquo; </strong><b>Current Servicing Type</b> <strong>-</strong> {this.state.currentServicingType}            
       </div>
        <div className="bauForms">
           <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p>
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
           <br></br>
              <form>
               <input type="text" placeholder="User ID" disabled={this.state.disableUserID} value={this.state.userId} onChange={this.userIDhandleChange} /> 
               <span className='orColor'>Or</span>
               <input type="text" disabled={this.state.disableGUID} placeholder="GUID" value={this.state.guid} onChange={this.guidHandleChange} /> 
               <div className="bttns">
                  <button onClick={this.onSearch}>Search</button>
                  <button onClick={this.enrollNew}>Enroll New</button>
                  <button onClick={this.onReset}>Clear</button>
               </div>   
            </form>
        </div>
      </div>
    );
  }
}
 
export default cc90;