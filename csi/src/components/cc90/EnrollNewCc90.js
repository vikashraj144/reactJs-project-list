import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
import Utils from "../../utils/Utils";
import * as Constants from '../../utils/Constants';

class EnrollNewCc90 extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentServicingType: 'CC90',
            userId:'',
            fullName:'',
            socialNumber:'',
            class:'error',
            validated: false,
            errorMsg:''
        }
        this.userIDhandleChange = this.userIDhandleChange.bind(this);
        this.fullNamehandleChange = this.fullNamehandleChange.bind(this);
        this.onReset = this.onReset.bind(this);
    }

    fullNamehandleChange = (event) => {
        this.setState({fullName:event.target.value,validated:false});
        let valid = Utils.regExAlphabetsOnly(event.target.value);
        if(valid){
            if(event.target.value.length === 0){
                this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.FULLNAME_VALIDATION});
              }else{
                this.setState({fullName: event.target.value,validated:true,class:'',errorMsg:''});
              }  
        }else{
                this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.ONLY_CHARCTER_VALIDATION});
        }
    }  

    /**
	 * Function to handle User ID input
	 */
	userIDhandleChange = (event) => {
		// Removing spaces
    let userIDValue = Utils.trimSpaces(event.target.value);
    
    let disableGUIDVal = (event.target.value.length)?true:false;
		this.setState({ userId: userIDValue,validated:false,disableGUID:disableGUIDVal });
        let valid = Utils.regExAlphaNumeric(userIDValue);
		if(valid) {
			if (userIDValue.length >= 5 && userIDValue.length <= 20) {
                this.setState({ validated: true, class:'', errorMsg:''});
                if(!Utils.regExOneAlphabetsAtleastOneNumber(userIDValue)){
				this.setState({class: 'error', validated: false, errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR });
                }                
			} else if (userIDValue.length === 0) {
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION});
			} else {
				this.setState({ class: 'error', validated: false, 
					errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
			}
        }
        
  }

    onMainSearch = () => {
        this.props.history.push('/csi/cc90');
    }
    
    onReset = (event) => {
        event.preventDefault();
        this.setState({fullName:'',userId:'',errorMsg:''});
    }

    onEnroll = (event) =>{
        console.log(this.state.validated);
        
        event.preventDefault();    
        if(this.state.fullName.length === 0 || this.state.userId.length === 0){
            this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.ENTER_DETAILS_VALIDATION_ERROR});
            event.preventDefault();    
    	    } else if(this.state.validated) {
            this.props.history.push('/csi/cc90/profile',{enrollMsg:Constants.ERROR_MSGS.ENROLL_SUCCESS})
          }
    }

    render(){
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Enroll New Customer CC90</h1>
        	</div> 
        <div className="bauForms">
            <form className="enroll-new">
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
<br></br>
               <input type="text" placeholder="Full Name" value={this.state.fullName} className="enroll" onChange={this.fullNamehandleChange}/> 
               <input type="text" placeholder="User ID" value={this.state.userId} className="enroll" onChange={this.userIDhandleChange}/> 
               <div className="bttns">
                  <button disabled={!this.state.validated} onClick={this.onEnroll}>Enroll</button>
                  <button onClick={this.onReset}>Clear</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
               </div>   
            </form>
        </div>
        </div>
        )
    }
}

export default EnrollNewCc90;