import React,{Component} from "react";
// import {Link} from "react-router-dom";
import {Table} from "react-bootstrap";

import ReactTable from "react-table";
import "react-table/react-table.css";

class IfscUserList extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentServicingType: 'AAH',
            
        }
    // this.onRowClick = this.onRowClick.bind(this);

    }

    onMainSearch = (event) => {
        this.props.history.push('/csi/ifsc');
    } 
    
    onRowClick = (event,d) => {
        debugger;
        // console.log(d);
        console.log(event.target.value);
        // this.props.history.push('/csi/ifsc/profile',{userId:'vverma'});
    }  

    render(){
        const { data } = this.state;
        const columns = [
            {
                Header: 'First Name',
                accessor: 'fName'
              },
              {
                Header: 'Last Name',
                accessor: 'lName'
              },
              {
                Header: 'Date of Birth',
                accessor: 'birthDate'
              },{
            Header: 'Email ID',
            accessor: 'emailId'
          },
          {
            Header: 'GUID',
            accessor: 'guid',
            width:275
          },  {
            Header: 'Zip Code',
            accessor: 'zipCd'
          }
        ]
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Select User ID</h1>
        	</div> 
            {/* <div className="profileForms"> */}
        <div className="maring-leftRight15">
        {/* <Table striped bordered condensed hover>
  <thead>
  <tr>
  <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Date of Birth</th>
      <th>Email ID</th>
      <th>GUID</th>
      <th>Zip Code</th>
    </tr>
  </thead>
  <tbody>
  <tr onClick = {() => {
          debugger;
          console.log("test");
          
      }
     } >
  <th>1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>01-31-1979</td>
      <td>vverma14@ampf.com</td>
      <td>ffffbfffaabd4c1caafece5b5b9ef46c</td>
      <td>43567</td>
    </tr>
    <tr>    
    <th>2</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>01-31-1979</td>
      <td>raghu@ampf.com</td>
      <td>ffffbfffaabd4c1caafece5b5b9ef46c</td>
      <td>43567</td>
    </tr>
    <tr>

      <td>Mark</td>
      <td>Otto</td>
      <td>01-31-1979</td>
      <td>glenn@ampf.com</td>
      <td>ffffbfffaabd4c1caafece5b5b9ef46c</td>
      <td>43567</td>
    </tr>
  </tbody>
</Table> */}
        <ReactTable
          showPagination={false}
          data={[
            {
            "fName": "Jeremy",
            "lName": "Hicke",
            "birthDate": "01-31-1979",
            "emailId": "rajeev.rajaboyina@ampf.com",
            "guid": "ffffbfffaabd4c1caafece5b5b9ef46c",
            "zipCd": "34711"
         },
            {
            "fName": "Srikanth Rao123",
            "lName": "Ambeer",
            "birthDate": "05-16-1980",
            "emailId": "rajeev.rajaboyina@ampf.com",
            "guid": "ffffbfffc17445ed9f7f56345395e6b3",
            "zipCd": "34765"
         }
      ]}
          columns={columns}
          defaultPageSize={5}
          className="-striped -highlight"
          getTrProps={(state, rowInfo) => ({
            onClick: () => {
                console.log(rowInfo,'rowInfo');
                this.props.history.push(`/csi/ifsc/profile`,{userId:'vikash'})

            }
            // this.props.history.push(`/db/${entity.id}/${rowInfo.original.__id}`)
          })}
        />            
        </div>
        {/* </div> */}
        <br></br>
                <div className="center">
                        <button onClick={this.onMainSearch}>Main Search</button>
                </div>
        </div>
        )
    }
}

export default IfscUserList;