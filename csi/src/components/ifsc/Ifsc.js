import React, { Component } from "react";
import Axios from "axios";

import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
import * as Constants from '../../utils/Constants';
import Utils from '../../utils/Utils';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { loadIfscFormData } from "./../../actions/dataAction"
import ConfigService from "../../api/ConfigService";

class Ifsc extends Component {
   
constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'IFSC',
        userId:'',
        class:'error',
        emailId:'',
		    disableUserId: false,
        validated:false,
		    disableEmail: false,
        errorMsg:''
    }
    
    this.userIdhandleChange = this.userIdhandleChange.bind(this);
    this.emailHandleChange = this.emailHandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  componentDidMount = () => {

		Utils.pLoader();
		Utils.closeLoader();
  }
    
  emailHandleChange(event) {
    let disableUserIdVal = (event.target.value.length)?true:false;
    this.setState({ validated:false,emailId: event.target.value,disableUserId:disableUserIdVal });
    let inputVal = event.target.value;
    let emailRegExp = Utils.emailRegExp(inputVal); 
    if(inputVal.length>=5 && inputVal.length <= 254 && emailRegExp){
      this.setState({validated:true,class:'',errorMsg:''});
    }else if(inputVal.length === 0){
      this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.ENTER_EMAIL_SSN});
    }else{
      this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.INVALID_EMAIL});
    }    
  }

  userIdhandleChange = (event) => {
    let inputVal = event.target.value;
    
    let disableEmailVal = (inputVal.length)?true:false;
    this.setState({ validated:false,userId: inputVal,disableEmail:disableEmailVal });
    let valid = Utils.regExAlphaNumeric(inputVal);
		if(valid) {
      if(inputVal.length>=5 && inputVal.length <= 20){
        this.setState({userId: inputVal,validated:true,class:'',errorMsg:''});
        if(!Utils.regExOneAlphabetsAtleastOneNumber(inputVal)){
          this.setState({class: 'error',errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR,validated:false});
        }
      }else if(inputVal.length === 0){
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION,validated:false});
      }else{
        this.setState({ class: 'error',errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR,validated:false });
      }    
    }else{
      this.setState({validated:false})
    }
    console.log(this.state.validated);
    
  }

  onSearch = (event) =>{
    console.log(this.state.validated,'validated');
    
    Utils.pLoader();
    event.preventDefault();   
    if(this.state.userId.length === 0 && this.state.emailId.length === 0){
      Utils.closeLoader();
      this.setState({class:'error',errorMsg: "Enter User ID or Email Address."});
    }else if(this.state.validated){
    if(this.state.emailId){
      this.props.history.push('/csi/ifsc/user-list',{userId:this.state.userId})
    }else{
      const param = {
        userId:this.state.userId
      }
      fetch(Constants.e1BaseURL + '/csi/v1.0/ifsc/client/profile',
            {
          method: `POST`,
          credentials: `include`,
                  headers: {
                      'Content-Type': 'application/json',
                      'apiToken': window.sessionStorage.getItem("apiToken"),
                      'SMSESSION': window.sessionStorage.getItem("SMSESSION"),
                      'selectedBU': 'CTI'
                  },
                  body: JSON.stringify({ 'userId': this.state.userId })
            }).then((res) => {
				if(res.status === 200) {
              
              res.json().then((response) => {
                console.log('response --',response);
                if((Object.keys(response)[0]).toLowerCase() === 'users') {
                  //Multiple Users
                  this.props.loadIfscFormData(response);
                  Utils.closeLoader();
                              this.props.history.push('/ifsc/aah-user-list');
                          } else {                            
                              //Single User
                              if(response.errorCode){
                                console.log("response.errorCode",response.errorCode);
                                // alert(response.errorMessage);
                        				this.setState({class: 'error', errorMsg: response.errorMessage});

                              }else{
                                console.log("ifsc search result completed");
                                // Utils.pLoader();
                                this.props.loadIfscFormData(response);
                                Utils.closeLoader();
                                this.props.history.push('/csi/ifsc/profile'/* ,{userId: jsonData.Identity_Profile[0][0].value} */)
                              }
                          }
                })
              } else {
                res.json().then((response) => {
                  // this.setState({class: 'error', errorMsg: response.errorMessage});
                  this.setState({class: 'error', errorMsg:"User profile not found in SSO system"});
                  console.log("response due to----", response);
                  Utils.closeLoader();
                })
              }
            })
            .catch((error) => {
                console.log("IFSC error----", error);
                Utils.closeLoader();
            })
      // this.props.history.push('/csi/ifsc/profile',{userId:this.state.userId})
    }
  }else{
    Utils.closeLoader();
  }

  }

  onReset = (event) => {
    event.preventDefault();    
    this.setState({userId:'',emailId:'',errorMsg:'',disableEmail:false,disableUserId:false})

  }

  render() {
    let message;
    if(this.state.errorMsg){
      message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
      if(this.props.location.state){
        this.props.location.state.msg = "";
      }
    }else if(this.props.location.state){
      message = <MessageBar errorMsg={this.props.location.state.msg} className="success"/>
    }
    return (
      <div>
        <HeaderTab />
       <div className="bauSelect align-center">
          <strong>&raquo; </strong><b>Current Servicing Type</b> <strong>-</strong> {this.state.currentServicingType}            
       </div>
        <div className="bauForms">
           <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p>
           {message}
           <br></br>
              <form id='userForm'>
               <input type="text" placeholder="User ID" disabled={this.state.disableUserId} value={this.state.userId} onChange={this.userIdhandleChange} autoFocus/> 
               <span className='orColor'>Or</span>
               <input type="text" placeholder="Email Address" disabled={this.state.disableEmail} value={this.state.emailId} onChange={this.emailHandleChange} /> 
               <div className="bttns">
                  <button onClick={this.onSearch}>Search</button>
                  <button onClick={this.onReset}>Clear</button>
               </div> 
        <a className="none-core-link" href="https://financialprofessional-emp.qa.ampf.com/ifscintra/authreg/MainMenu.jsp">ISFC Non-Core </a>

            </form>
        </div>
      </div>
    );
  }
}
 
const mapStateToProps = (state) => {
	return {
		ifscFormData: state.ifscFormData
	}	
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadIfscFormData
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Ifsc);