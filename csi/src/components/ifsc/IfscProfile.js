import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
// import InputMessageBar from "../shaired/InputMessageBar";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Constants from '../../utils/Constants';
import Utils from '../../utils/Utils';

import { loadIfscFormData } from "./../../actions/dataAction"
class IfscProfile extends Component{

    constructor(props){
        super(props);
        this.state = {
            class:'',
            errorMsg:'',
            isSubscribe:'Unsubscribe',
            display:'',
            firstUser: {
                userId : '', userGuid : '', lastLoginDate : '', userStatus : '', pwdStatus : '', 
                isSubscribed : '', subscribedServices : '', dateOfBirth : '', 
                firstName : '',emailId : '', lastName : '', emailId : '', zipCd: '' 
            },
            secondUser: {
                userId : '', userGuid : '', lastLoginDate : '', userStatus : '', pwdStatus : '', 
                isSubscribed : '', subscribedServices : '', dateOfBirth : '', 
                firstName : '',emailId : '', lastName : '', emailId : '', zipCd: '' 
            },
        };
    }
    componentDidMount = () => {

        const { ifscFormData } = this.props;
        console.log('-----',ifscFormData.identityProfile);
        if(!ifscFormData.identityProfile){
            this.props.history.push('/csi/ifsc');
        }
        if(ifscFormData.length !== 0) {
            //Check for Multiple User Id's
            if((typeof ifscFormData.Users) != 'undefined') {

                if(ifscFormData.Users.length === 3) {
                    //3 User Id's
                } else if(ifscFormData.Users.length === 2) {
                    //Multiple User id
                    let firstUser, secondUser;
                    if(ifscFormData.Users[0].identityProfile[1].value === ifscFormData.selectedGuid) {
                        firstUser = this.setFirstUserValues(ifscFormData.Users[0]);
                        secondUser = this.setSecondUserValues(ifscFormData.Users[1]);
                    } else {
                        firstUser = this.setFirstUserValues(ifscFormData.Users[1]);
                        secondUser = this.setSecondUserValues(ifscFormData.Users[0]);
                    }
                    this.setState({ firstUser, secondUser });
                }
            } else {
                //Single User id
                let firstUser = this.setSingleUserValues(ifscFormData);
                this.setState({ firstUser });
            }
        } else {
            this.props.history.push('/csi/ifsc');
        }
    }

    
    /**
     * Function to set data to First User in case of Multiple user Id's
     */
    setFirstUserValues = (data) => {

        let firstUser = this.state.firstUser;
        firstUser.userId = data.identityProfile[0].value;
        firstUser.userGuid = data.identityProfile[1].value;
        firstUser.userStatus = data.identityProfile[2].value;
        firstUser.lastName = data.businessProfile.lastName;
        return firstUser;
    }

    /**
     * Function to set data to Second User in case of Multiple user Id's
     */
    setSecondUserValues = (data) => {

        let secondUser = this.state.secondUser;
        let firstUser = this.state.firstUser;
        secondUser.userId = data.identityProfile[0].value;
        secondUser.userGuid = data.identityProfile[1].value;
        secondUser.userStatus = data.identityProfile[2].value;
        secondUser.lastName = data.businessProfile.lastName;
        return secondUser;
    }

    /**
     * Function to set data to FirstUser in case of Single User Id
     */
    setSingleUserValues = (data) => {
        let firstUser = this.state.firstUser;
        if(data.identityProfile){
            firstUser.userId = data.identityProfile[0].value;
            firstUser.userGuid = data.identityProfile[1].value;
            firstUser.lastLoginDate = data.identityProfile[2].value;
            firstUser.userStatus = data.identityProfile[3].value;
            firstUser.pwdStatus = data.identityProfile[4].value;
            firstUser.isSubscribed = data.identityProfile[5].value;
            firstUser.subscribedServices = data.identityProfile[6].value;
            firstUser.dateOfBirth = data.businessProfile.dob;
            firstUser.firstName = data.businessProfile.firstName;
            firstUser.emailId = data.businessProfile.emailId;
            firstUser.lastName = data.businessProfile.lastName;
            firstUser.zipCd = data.businessProfile.zipCd;
            return firstUser;
        }
    }
    onMainSearch = () => {
        this.props.history.push('/csi/ifsc');
    }
    
    enrollNew = () => {
        this.props.history.push('/csi/ifsc-enroll-new');
    }

    subscribe = () => {
        
        // redirect to ifsc with unsubscribe message
        if(this.state.isSubscribe ==='Unsubscribe'){
            if (window.confirm("Are you sure you want to unsubscribe the user ?")) { 
                let str = this.state.firstUser.subscribedServices;
                let subscribedServicesCd = str.substr(str.indexOf(",") + 1);
                subscribedServicesCd = subscribedServicesCd.replace(/\s/g,'');
        
                // calling api for unSubscribe
                fetch(Constants.e1BaseURL + '/csi/v1/ifsc/unSubscribe',
                {
                    method: `DELETE`,
                    credentials: `include`,
                    headers: {
                        'Content-Type': 'application/json',
                        'apiToken': window.sessionStorage.getItem("apiToken"),
                        'SMSESSION': window.sessionStorage.getItem("SMSESSION"),
                        'selectedBU': 'CTI'
                    },
                    body: JSON.stringify({ 'userGuid': this.state.firstUser.userGuid,'subscribedServicesCd':`${subscribedServicesCd}` })
                }).then((res) => {
                    res.json().then((response) => {
                        // getting response here 
                        Utils.pLoader();
                        console.log('response',response);
                        // this.setState({isSubscribe:'Subscribe'});
                        this.setState({isSubscribe:'',display:'display-non'});
                        this.props.history.push('/csi/ifsc',{msg:'Unsubscribe operation Successful'});
                        Utils.closeLoader();
                    })

                }).catch((error) => {
                    console.log("IFSC error----", error);
                    this.setState({class: 'error', errorMsg: 'Unsubscribe operation Failed'});
                    Utils.closeLoader();
                })
              }
        }
    }

    // deleteUserID = () =>{
    //     alert('Are you sure you want to delete user ?');
    // }
    toggleSubscribe = () => {
        this.setState({class:'',errorMsg: ""});
    }

    /**
     * Function on Reset Password
     */
    onResetPassword = (event) => {
        Utils.pLoader();
        if (window.confirm("Are you sure you want to reset password?")) {
            
            //Method to call on Reset Password
            fetch(Constants.e1BaseURL + '/csi/v1/resetClientPwd', 
            {
                method: `POST`,
                //credentials: `include`,//To comment in local
                headers: {
                    'Content-Type': 'application/json',
                    'apiToken': window.sessionStorage.getItem("apiToken"),
                    'SMSESSION': window.sessionStorage.getItem("SMSESSION"), //To remove in E1
                    'selectedBU': 'CTI'
                },
                body: JSON.stringify({ "userId": this.state.firstUser.userId })
            }).then((res) => {
                if(res.status === 200) {
                    res.json().then((response) => {
                        console.log('response',response);
                        
                        let newTempPwd = (response.identityProfile[2].value).split('').join(' ');
                        this.setState({ class: 'error', pass: newTempPwd,
                            errorMsg: "Reset Password Successful - New password is: "});
                        Utils.closeLoader();
                    })
                    
                } else {
                    res.json().then((response) => {
                        console.log("IFSC reset password Error----", response.errorCode);
                        this.setState({class:'error',errorMsg: "Error: password reset failed - try again."});
                        Utils.closeLoader();
                    })
                    
                }
            }).catch((error) => {
                console.log("IFSC Reset password error----", error);
                Utils.closeLoader();
            })
        } else {
            this.setState({class:"",errorMsg:"",pass:""});
        }
        event.preventDefault();    
        
    }

    render(){
        const isError = this.state.class;
        let message,messageUnsubscribe;
        if(this.state.pass){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }else{
            messageUnsubscribe = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
        }
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> IFSC Customer Information</h1>
            </div>    
                <div className="profileForms">
                        {/* 1st tab start */}
                        <table>
                        <tbody>
                        <tr className="profile-head-color">
                            <th>Security Identity Data</th>
                            <th></th>
                            <td>Enrolled Services</td> 
                        </tr>
                        <tr>
                            <td className="bold td1">User ID:</td>
                            <td className="td2">
                            {this.state.firstUser.userId}
                            </td> 
                            {/* <td className="td2">David</td>  */}
                            <td className="td3">{this.state.firstUser.subscribedServices}</td> 
                        </tr>
                        <tr>
                            <td className="bold td1">Status:</td>
                            <td className="td2">{this.state.firstUser.userStatus}</td> 
                        </tr>
                        <tr>
                            <td className="bold td1">GUID:</td>
                            <td className="td2">{this.state.firstUser.userGuid}</td> 
                        </tr>
                        <tr>
                            <td className="bold td1">Last Login:</td>
                            <td className="td2">{this.state.firstUser.lastLoginDate}</td> 
                        </tr>
                        <tr>
                            <td className="bold td1">Password Status:</td>
                            <td className="td2">{this.state.firstUser.pwdStatus}</td> 
                        </tr>
                        </tbody>
                        </table>
                    
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            <div className="profile-head-color">
                    <span className="profile-head-left">Service Functions</span>
            </div>
            {messageUnsubscribe}

            <table className="flot-left">
            <tbody>
      <tr>
        <td className="bold">Customer Name:</td>
        <td>{this.state.firstUser.firstName} {this.state.firstUser.lastName} </td>
      </tr>
      <tr>
        <td className="bold">Email Address:</td>
        <td>{this.state.firstUser.emailId}</td>
      </tr>
      <tr>
        <td className="bold">DOB:</td>
        <td>{this.state.firstUser.dateOfBirth}</td>
      </tr>
      <tr>
        <td className="bold">Zip:</td>
        <td>{this.state.firstUser.zipCd}</td>
      </tr>
      </tbody>
    </table>
    <table className="flot-left-left">
    <tbody>
      <tr>
        <td>
        <button className={this.state.display} onClick={this.subscribe}>Unsubscribe</button>
        </td>
      </tr>
{/*       
      <tr>
        <td className="maring-left15">
        <span className="flot-left2per">
        <a className="none-core-link" href="https://financialprofessional-emp.qa.ampf.com/ifscintra/authreg/MainMenu.jsp">ISFC Non-Core </a>
        </span>
        </td>
      </tr> */}
      </tbody>
    </table>
            <div>
              
            </div>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
                {message}
                <form>
                 <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button onClick={this.onMainSearch}>Main Search</button>
                                    <button onClick={this.onResetPassword}>Reset Password</button>
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}
const mapStateToProps = (state) => {
	return {
        ifscFormData: state.ifscFormData
    }
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadIfscFormData
}, dispatch);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(IfscProfile);