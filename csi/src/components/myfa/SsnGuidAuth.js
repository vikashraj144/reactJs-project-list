import React, { Component } from "react";
// import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
// import Header from "../../Header";
import Utils from "../../utils/Utils";
 
class SsnGuidAuth extends Component {

constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'MYFA',
        client_number:'',
        ssn:'',
        class:'error',
        errorMsg:'',
        isAdminCheck:false
    }
    
    this.ClientNumberHandleChange = this.ClientNumberHandleChange.bind(this);
    this.SSNhandleChange = this.SSNhandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }


  ClientNumberHandleChange = (event) => {
    let valid = Utils.regExNumber(event.target.value);
        if(valid){
          if(event.target.value.length === 12){
            this.setState({client_number: event.target.value,class:'',errorMsg:''});
          }
          else if(event.target.value.length < 12){
            this.setState({class:'error',errorMsg: "Enter the Client ID numbers"});
          }else{
            this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
          }
        }else{
          this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
        }
  }

  SSNhandleChange = (event) => {
    let valid = Utils.regExNumber(event.target.value);
        if(valid){
          if(event.target.value.length >= 1){
            this.setState({ssn: event.target.value,class:'',errorMsg:''});
          }
          else{
            this.setState({class:'error',errorMsg: "SSN can't be more than 9 characters"});
          }
        }else{
          this.setState({class:'error',errorMsg: "SSN entered is not valid"});
        }
    // if(event.target.value.length <= 9){
    //   this.setState({ssn: event.target.value,class:'',errorMsg:''});
    // }else{
    //   this.setState({class:'error',errorMsg: "SSN can't be more than 9 characters"});
    // }
  }

  onSearch = (event) =>{
    if(this.state.ssn.length === 0 || this.state.client_number.length ===0){
      this.setState({class:'error',errorMsg: "Enter detail."});
      event.preventDefault();    
    }else{
      this.props.history.push('/csi/myfa/profile',{
        isAdminCheck:this.state.isAdminCheck,
        userID:'David',
        firstName:'David',
        lastName:'Wilsan',
        dob:'01/01/1999',
        clientNumber:'1234 5678 2 4567'
      });
      event.preventDefault();    
    }
  }
  onReset = (event) => {
    document.getElementById("authForm").reset();
    event.preventDefault();    

  }
  onMainSearch = () => {
    this.props.history.push('/csi/');
  }
  onCheckbox = () =>{
    this.setState({isAdminCheck:(this.state.isAdminCheck===false)?true:false});
  }
  render() {
  let isAdmin = false;
  if(this.props.location.state && this.props.location.state.user === "vverma14"){
    isAdmin = true;
  }

    return (
      <div>
       <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Customer Authentication - MYFA</h1>
        	</div> 
        <div className="bauForms">
        <p className="align-center"><strong>&laquo; </strong>Current Service type My Account R3A<strong> &raquo; </strong></p>
        <p className="align-center"><strong>&laquo; </strong>
        Following authentication data is to be collected to validate the User ID to the Customer on phone. 
        <strong> &raquo; </strong></p>

            <form className="enroll-new" id="authForm">
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
        <br></br>
               {/* <input type="text" placeholder="Full Name" value={this.state.value} className="enroll" onChange={this.fullNamehandleChange}/>  */}
               {/* <input type="text" placeholder="User ID" value={this.state.value} className="enroll" onChange={this.userIDhandleChange}/>  */}
               <input type="text" placeholder="SSN/TIN" value={this.state.value} onChange={this.SSNhandleChange} /> 
               <span className='orColor'>AND</span>
               <input type="text" placeholder="Client Number" value={this.state.value} onChange={this.ClientNumberHandleChange}/> 
<span>
               {isAdmin && <input className="width-checkbox" onChange={this.onCheckbox} type="checkbox"></input>}
               {isAdmin && "One or more pieces of authentication data are unavailable. Check if you have positively and manually authenticated the caller to this User ID."}

</span>

               <div className="bttns">
                  <button onClick={this.onSearch}>Submit</button>
                  <button onClick={this.onReset}>Clear</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
                  {/* <span className="resetbtn" onClick={this.onReset}>Clear</span>  */}
               </div>   
            </form>

        </div>
        </div>
        
      </div>
    );
  }
}
 
export default SsnGuidAuth;