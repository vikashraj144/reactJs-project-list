import React, { Component } from "react";
import MessageBar from "../shaired/MessageBar";
import Utils from "../../utils/Utils";

class SnnTinAuth extends Component {
constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'MYFA',
        ssn:'',
        class:'error',
        errorMsg:''
    }
    this.SSNhandleChange = this.SSNhandleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  SSNhandleChange = (event) => {
    let valid = Utils.regExNumber(event.target.value);
        if(valid){
          if(event.target.value.length >= 1){
            this.setState({ssn: event.target.value,class:'',errorMsg:''});
          }
          else{
            this.setState({class:'error',errorMsg: "SSN can't be more than 9 characters"});
          }
        }else{
          this.setState({class:'error',errorMsg: "SSN entered is not valid"});
        }
  }

  onSubmit = (event) =>{
    if(this.state.ssn.length === 0){
      this.setState({class:'error',errorMsg: "Please enter required data before selecting Submit."});
      event.preventDefault();    
    }else{
      this.props.history.push('/csi/myfa/profile')
      event.preventDefault();    
    }
  }
  onReset = (event) => {
    document.getElementById("authForm").reset();
    event.preventDefault();    

  }
  onMainSearch = () => {
    this.props.history.push('/csi/');
  }

  render() {
    return (
      <div>
       <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Customer Authentication - MYFA</h1>
        	</div> 
        <div className="bauForms">
        <p className="align-center"><strong>&laquo; </strong>Current Service type My Account R3A<strong> &raquo; </strong></p>
        <p className="align-center"><strong>&laquo; </strong>
        Following authentication data is to be collected to validate the User ID to the Customer on phone. 
        <strong> &raquo; </strong></p>

            <form className="enroll-new" id="authForm">
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
        <br></br>
               <input type="text" placeholder="SSN/TIN" value={this.state.value} autoFocus onChange={this.SSNhandleChange}/> 

               <div className="bttns">
                  <button onClick={this.onSubmit}>Submit</button>
                  <button onClick={this.onReset}>Clear</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
               </div>   
            </form>

        </div>
        </div>
        
      </div>
    );
  }
}
 
export default SnnTinAuth;