import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";

class MyfaProfile extends Component{

    constructor(props){
        super(props);
        this.state = {
            title:' MYFA Customer Information ',
            class:'',
            pass:'',
            errorMsg:'',
            isSubscribe:'Unsubscribe',
            unEnrolldisplay:'',
            changeUserMsg:'',
            userID:'David',
            Data:'',
            enrolledServices: ['SSO Service' , 'Message Center' , 'Quicken' ]
        };
    }

    onMainSearch = () => {
        this.props.history.push('/csi/');
    }
    
    enrollNew = () => {
        this.props.history.push('/csi/myfa-enroll-new');
    }

    changeUserID = () => {
        this.props.history.push('/csi/myfa-change-user-id',{Data:this.state.Data});
    }

    subscribe = () => {

        if(this.state.isSubscribe ==='Unsubscribe'){
            if (window.confirm("Are you sure you want to unsubscribe")) { 
                this.setState({isSubscribe:'Subscribe',class:'success',errorMsg: "Client successfully Unsubscribed",pass:''});
                this.props.history.push('/csi/myfa-profile-subscribe',{userID:this.state.userID,UnSubscribedMsg:'Client successfully Unsubscribed'});
              }
        }else if(this.state.isSubscribe ==='Subscribe'){
            if (window.confirm("Are you sure you want to subscribe")) { 
                this.setState({isSubscribe:'Unsubscribe',class:'success',errorMsg: "Client successfully subscribed",pass:''});
              }
        }
    }

    onRestPassword = (event) =>{
        if (window.confirm("Are you sure you want to reset password?")) { 
            this.setState({class:'error',errorMsg:"Reset Password Successful - New password is: ",pass:'! 3 4 5 6 $ % #'});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();    
        
    }
    onUnenroll = (event) =>{
        if (window.confirm("Confirm unenroll from Total View.")) { 
            this.setState({class:'success',unEnrolldisplay:'display-non',errorMsg:"Success: client unenrolled from total view",pass:''});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();   
    }

    /**
     * 
     */
    handleQuickenTokenChange = () => {

        if(window.confirm("This will disable the user's Quicken token. Are you sure?")) {
            //Call unsubscribe service on gateway --pending
            let enrolledServices = this.state.enrolledServices;
            let index = enrolledServices.splice(this.state.enrolledServices.indexOf("Quicken"), 1);
            this.setState({ class:'success', 
                errorMsg: "Quicken token disabled", enrolledServices});
            
        } else {

        }
    }
    
    render(){

        console.log(this.state.unEnrolldisplay);
        
        let unEnrolldisplay  = this.props.location.state?this.props.location.state.unEnrolldisplay:this.state.unEnrolldisplay;
        
        let newUserID  = this.props.location.state?this.props.location.state.userID:this.state.userID;
        let firstName  = this.props.location.state?this.props.location.state.firstName:'David';
        let lastName  = this.props.location.state?this.props.location.state.lastName:'Wilsan';
        let dob  = this.props.location.state?'01/10/1999':'01/01/1999';
        let dobDisplay = '',isAdminCheckVal=false;
        if(this.props.location.state){
            dobDisplay = this.props.location.state.dob?'':'display-non';
            isAdminCheckVal = this.props.location.state.isAdminCheck;
        }
        // let dob  = this.props.location.state?this.props.location.state.dob:'01/01/1999';
        // let socialNumber  = this.props.location.state?this.props.location.state.socialNumber:'***-**-1111';
        let clientNumber  = this.props.location.state?this.props.location.state.clientNumber:'1234 5678 2 4567';
        let emailId  = this.props.location.state?this.props.location.state.emailId:'David';
        
        this.state.Data = {firstName:firstName,lastName:lastName,dob:dob,clientNumber:clientNumber,unEnrolldisplay:unEnrolldisplay}

        let showResetPassBtnClass = (this.state.isSubscribe==='Unsubscribe')?'':'display-non'
        let resetMessage, message,subscribeMsg;
        if(this.state.pass!=''){
            resetMessage = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }else if(this.state.errorMsg.length ==0 && this.props.location.state){
            if(this.props.location.state.msg){
                message = <MessageBar errorMsg={this.props.location.state.msg} className="success"/>
            }else if(this.props.location.state.subscribeMsg){
                subscribeMsg = <MessageBar errorMsg={this.props.location.state.subscribeMsg} className="success"/>
            }else{
                resetMessage = <MessageBar errorMsg={this.props.location.state.enrolledMsg} className="success"/>
            }
        }else if(this.state.pass==''){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }

        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> {this.state.title}</h1>
            </div>    

                <div className="profileForms">
                
        <table>
            <tbody>
        <tr className="profile-head-color">
            <th>Security Identity Data</th>
            <th></th>
            <td>Enrolled Services</td> 
        </tr>
        <tr>
            <td className="bold td1">User ID:</td>
            <td className="td2">{newUserID} </td> 
            <td className="td3">{this.state.enrolledServices.join(", ")}</td>
        </tr>
        <tr>
            <td className="bold td1">Status:</td>
            <td className="td2">Active</td> 
        </tr>
        <tr>
            <td className="bold td1">GUID:</td>
            <td className="td2">wertyuiosdfghjklxcvbn345678</td> 
        </tr>
        <tr>
            <td className="bold td1">Last Login:</td>
            <td className="td2">01/27/2018</td> 
        </tr>
        <tr>
            <td className="bold td1">Password Status:</td>
            <td className="td2">Active</td> 
        </tr>
        </tbody>
        </table>
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            <div className="profile-head-color">
                    <span className="profile-head-left">My Financial Account Information</span>
                    {/* <span className="profile-head-right">Total View Information</span> */}
            </div>
        {message}
        {subscribeMsg}
        <div className="profile-head-color">
                    <span className="profile-head-left">Investment Accounts</span>
                    <span className="myfa-head-right">
                    Total View Information
                        </span>
            </div>
            <div className="divTable">
                <table>
                    <tbody>
                <tr>
                    <td className="bold td1">Enrolled date:</td>
                    <td className="td2">01/22/1996</td> 
                    <td className="myfa-td3">
                <button className={this.state.unEnrolldisplay} onClick={this.onUnenroll}>Unenroll Total View</button>                
                    
                    {/* Enrolled date 04/02/1988   */}
                    </td> 
                </tr>
                <tr>
                    <td className="bold td1">User Type:</td>
                    <td className="td2">Person</td> 
                    <td className="myfa-td3-enroll">
                        <p className="myfa-profile-padding">{unEnrolldisplay?"Not Enrolled":"Enrolled date 04/02/1988"}</p>                    
                    </td> 
                </tr>
                <tr>
                    <td className="bold td1">Client Number:</td>
                    <td className="td2">{clientNumber}</td>                     
                </tr>
                <tr>
                    <td className="bold td1">SSN/TIN:</td>
                    <td className="td2">{(isAdminCheckVal===true?'999-99-9999':'***-**-9999')}</td> 
                </tr>

                <tr>
                    <td className="bold td1">First Name:</td>
                    <td className="td2">{firstName}</td> 
                </tr>
                <tr>
                    <td className="bold td1">Last Name:</td>
                    <td className="td2">{lastName}</td> 
                </tr>
                <tr className={dobDisplay}>
                    <td className="bold td1">DOB:</td>
                    <td className="td2">{dob}</td> 
                </tr>
                </tbody>
                </table> 
                <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                <button onClick={this.subscribe}>{this.state.isSubscribe}</button>
                                <button onClick={this.changeUserID}>Change User ID</button>
                                <button onClick={this.handleQuickenTokenChange} 
                                    disabled={this.state.enrolledServices.indexOf("Quicken") > -1 ? false : true}
                                    className={this.state.enrolledServices.indexOf("Quicken") > -1 ? "" : "btn-gray"} >Disable Quicken Token</button>
                            </center>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
                {resetMessage}
                <form>

                 <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button className={showResetPassBtnClass} onClick={this.onRestPassword}>Reset Password</button>
                                    <button onClick={this.onMainSearch}>Main Search</button>
                                    <button onClick={this.enrollNew}>Enroll New Client</button>
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}

export default MyfaProfile;