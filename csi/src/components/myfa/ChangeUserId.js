import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
// import message from "../../shaired/message";
import * as Constants from '../../utils/Constants';

const userIDMsg="User ID must contain 5-20 alphanumeric or special characters.  The following characters are not permitted \" \" ' & < > / \ * ? ) ( ^ blank ";

class ChangeUserId extends Component{

    constructor(props){
        super(props);
        this.state = {
            title:' MYFA Customer Information ',
            class:'',
            pass:'',
            errorMsg:'',
            userID:'',
            isSubscribe:'Unsubscribe'
        };
        this.userIDhandleChange = this.userIDhandleChange.bind(this);

    }
    
    userIDhandleChange = (event) => {
        let RegExpression = /^[(!@$-%^)a-zA-Z0-9 \s]*$/; 
        console.log(event.target.value);
         
        // removing space
        let inputUserIDVal = event.target.value.replace(/\s/g, "");
        if(inputUserIDVal.length>=5 && inputUserIDVal.length <= 20 && RegExpression.test(inputUserIDVal)){
          this.setState({class:'',errorMsg:''});
          this.state.userID = event.target.value;
        }else if(inputUserIDVal.length === 0){
          this.setState({class:'error',errorMsg: "Enter User ID."});
        }else{
            this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR});

        //   this.setState({class:'error', Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR});
        }    
    }

    onCancel = () => {
        this.props.history.push('/csi/myfa/profile');
    }

    onChangeUserID = (event) => {
        if(this.state.userID.length <= 1){
            this.setState({class:'error',errorMsg: "Please enter userID"});            
        }else{
            let Data = {
            msg:'User ID successfully changed',
            userID:this.state.userID,
            firstName:this.props.location.state.Data.firstName,
            lastName:this.props.location.state.Data.lastName,
            unEnrolldisplay:this.props.location.state.Data.unEnrolldisplay,
            dob:this.props.location.state.Data.dob,
            clientNumber:this.props.location.state.Data.clientNumber
        };
            this.props.history.push('/csi/myfa/profile',Data);
        }
        event.preventDefault();
    }
    
    render(){

        console.log(this.props.location.state.Data.firstName,'----------');
        
        let resetMessage, message;
        if(this.state.pass!=''){
            resetMessage = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }else{
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }

        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> {this.state.title}</h1>
            </div>    
                <div className="profileForms">
        <form> 
            {message}
        <table>
            <tbody>
        <tr className="profile-head-color">
            <th>Security Identity Data</th>
            <th></th>
            <td>Enrolled Services</td> 
        </tr>
        <tr>
            <td className="bold td1">User ID:</td>
            <td className="td2">David &nbsp;&nbsp;&nbsp; 
            <input type="text" value={this.state.value} className="enroll change-userID-input" onChange={this.userIDhandleChange}/> 
            
            </td> 
            <td className="td3">SSO Service , Quicken</td> 
        </tr>
        <tr>
            <td className="bold td1">Status:</td>
            <td className="td2">Active</td> 
        </tr>
        <tr>
            <td className="bold td1">GUID:</td>
            <td className="td2">wertyuiosdfghjklxcvbn345678</td> 
            
        </tr>
        <tr>
            <td className="bold td1">Last Login:</td>
            <td className="td2">01/27/2018</td> 
        </tr>
        <tr>
            <td className="bold td1">Password Status:</td>
            <td className="td2">Active</td> 
        </tr>
        </tbody>
        </table>
        <br></br>
        <span className="myfa-cguserid-footer">
        <p>Enter User ID and select Change User ID <br></br>
        {userIDMsg}
        </p>
        </span>
        <br></br>
        <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button onClick={this.onChangeUserID}>Change User ID</button>
                                    <button onClick={this.onCancel}>Cancel</button>
                            </center>
                        </div>
                    </div>
        </div> 
        </form>

                </div>
      </div>
        );
    }
}

export default ChangeUserId;