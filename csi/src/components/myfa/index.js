import Myfa from './Myfa';
import ChangeUserId from './ChangeUserId';
import ClientNumberAuth from './ClientNumberAuth';
import EnrollMyfa from './EnrollMyfa';
import MyfaProfile from './MyfaProfile';
import MyfaProfileSubscribe from './MyfaProfileSubscribe';
import SnnTinAuth from './SnnTinAuth';
import SsnGuidAuth from './SsnGuidAuth';

export {
    Myfa,
    ChangeUserId,
    ClientNumberAuth,
    EnrollMyfa,
    MyfaProfile,
    MyfaProfileSubscribe,
    SnnTinAuth,
    SsnGuidAuth,
}