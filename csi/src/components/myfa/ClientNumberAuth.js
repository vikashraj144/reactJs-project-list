import React, { Component } from "react";
import MessageBar from "../shaired/MessageBar";
import Utils from "../../utils/Utils";
 
class ClientNumberAuth extends Component {

constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'MYFA',
        client_number:'',
        ssn:'',
        class:'error',
        errorMsg:''
    } 
    this.ClientNumberHandleChange = this.ClientNumberHandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  ClientNumberHandleChange = (event) => {
    let valid = Utils.regExNumber(event.target.value);
    if(valid){
      if(event.target.value.length === 12){
        this.setState({client_number: event.target.value,class:'',errorMsg:''});
      }
      else if(event.target.value.length < 12){
        this.setState({class:'error',errorMsg: "Enter the Client ID numbers"});
      }else{
        this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
      }
    }else{
      this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
    }

  }

  onSearch = (event) =>{
    if(this.state.ssn.length === 0 && this.state.client_number.length ===0){
      this.setState({class:'error',errorMsg: "Client Number must contain 12 numeric character."});
      event.preventDefault();    
    }else{
      this.props.history.push('/csi/myfa/profile')
      event.preventDefault();    
    }
  }
  onReset = (event) => {
    document.getElementById("authForm").reset();
    event.preventDefault();    

  }
  onMainSearch = () => {
    this.props.history.push('/csi/');
  }

  render() {
    return (
      <div>
       <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Customer Authentication - MYFA</h1>
        	</div> 
        <div className="bauForms">
        <p className="align-center"><strong>&laquo; </strong>Current Service type My Account R3A<strong> &raquo; </strong></p>
        <p className="align-center"><strong>&laquo; </strong>
        Following authentication data is to be collected to validate the User ID to the Customer on phone. 
        <strong> &raquo; </strong></p>

            <form className="enroll-new" id="authForm">
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
        <br></br>
               <input type="text" placeholder="Client Number" value={this.state.value} onChange={this.ClientNumberHandleChange}/> 
               <div className="bttns">
                  <button onClick={this.onSearch}>Submit</button>
                  <button onClick={this.onReset}>Clear</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
               </div>   
            </form>

        </div>
        </div>
        
      </div>
    );
  }
}
 
export default ClientNumberAuth;