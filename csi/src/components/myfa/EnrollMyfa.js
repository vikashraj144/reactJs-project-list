import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
import * as Constants from "../../utils/Constants";
import Utils from "../../utils/Utils";

import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

const userIDMsg="User ID must contain 5-20 alphanumeric or special characters.  The following characters are not permitted \" \" ' & < > / \ * ? ) ( ^ blank ";

class EnrollMyfa extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentServicingType: 'MYFA',
            userId:'',
            fullName:'',
            socialNumber:'',
            class:'error',
            // investmentAccountType:'Individual',
            isDateBlock: false,
            errorMsg:'',

            investmentAccountType:'Individual',
            firstName:'',
            lastName:'',
            dob:'01/01/1999',
            clientNumber:'',
            emailId:'',
            isEmailAccept:false,
            radioCheck: true,
            disableAccept: true,
            acceptBtn:'btn-gray'
        }

        //this.investmentAccountTypeChg = this.investmentAccountTypeChg.bind(this);
        this.ClientNumberHandleChange = this.ClientNumberHandleChange.bind(this);
        this.SSNhandleChange = this.SSNhandleChange.bind(this);
        this.firstNameHandlerChange = this.firstNameHandlerChange.bind(this);
        this.lastNamehandleChange = this.lastNamehandleChange.bind(this);
        this.emailHandleChange = this.emailHandleChange.bind(this);
        this.dobHandleChange = this.dobHandleChange.bind(this);
        this.userIDhandleChange = this.userIDhandleChange.bind(this);
        this.onIndividualChange = this.onIndividualChange.bind(this);
        this.onOrganizationChange = this.onOrganizationChange.bind(this);
    }
    
    dobHandleChange = (event) => {
        if(event.target.value.length > 0){
          this.setState({dob: event.target.value,class:'',errorMsg:''});
        }else{
          this.setState({class:'error',errorMsg: "dob can not be empty"});
        }
      }
      
    lastNamehandleChange = (event) => {
		    let valid = Utils.regExAlphabetsOnly(event.target.value);
        if(valid){
          if(event.target.value.length >= 1){
            this.setState({lastName: event.target.value,class:'',errorMsg:''});
          }
          else{
            this.setState({class:'error',errorMsg: "Name can not be empty"});
          }
        }else{
          this.setState({class:'error',errorMsg: "Name must contain only letters"});
        }
      }

      emailHandleChange = (event) => {
        var emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let inputVal = event.target.value;
        if(inputVal.length>=5 && inputVal.length <= 254 && emailRegExp.test(inputVal)){
          this.setState({emailId: event.target.value,class:'',errorMsg:''});
          if(event.target.value ==='vverma14@ampf.com'){
            this.setState({class:'',isEmailAccept:false,errorMsg: ""});
            // this.setState({class:'error',isEmailAccept:false,acceptBtn:'',errorMsg: "You must accept the email address to proceed."});
        }else{
            this.setState({class:'',isEmailAccept:true,errorMsg: ""});
        }
        }else if(inputVal.length === 0){
          this.setState({class:'error',errorMsg: "Enter the Email Address",pass:''});
        }else{
          this.setState({class:'error',errorMsg: "Email Address is invalid",pass:''});
        }    
      }

      firstNameHandlerChange = (event) => {
        let valid = Utils.regExAlphabetsOnly(event.target.value);
        if(valid){
          if(event.target.value.length >= 1){
            this.setState({firstName: event.target.value,class:'',errorMsg:''});
          }
          else{
            this.setState({class:'error',errorMsg: "Name can not be empty"});
          }
        }else{
          this.setState({class:'error',errorMsg: "Name must contain only letters"});
        }
      }

    SSNhandleChange = (event) => {
      let valid = Utils.regExNumber(event.target.value);
        if(valid){
          if(event.target.value.length >= 1){
            this.setState({socialNumber: event.target.value,class:'',errorMsg:''});
          }
          else{
            this.setState({class:'error',errorMsg: "SSN can't be more than 9 characters"});
          }
        }else{
          this.setState({class:'error',errorMsg: "SSN entered is not valid"});
        }
      }

  ClientNumberHandleChange = (event) => {
    let valid = Utils.regExNumber(event.target.value);
        if(valid){
          if(event.target.value.length === 12){
            this.setState({clientNumber: event.target.value,class:'',errorMsg:''});
          }
          else if(event.target.value.length < 12){
            this.setState({class:'error',errorMsg: "Enter the Client ID numbers"});
          }else{
            this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
          }
        }else{
          this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
        }

    // if(event.target.value.length === 12){
    //   this.setState({clientNumber: event.target.value,class:'',errorMsg:''});
    // }else{
    //   this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
    // }
  }

    /* investmentAccountTypeChg = (event) =>{
        this.setState({investmentAccountType:event.target.value});
        console.log(event.target.value);
        if(event.target.value ==='Individual'){
            this.setState({isDateBlock: false})
        }else{
            this.setState({isDateBlock: true})
        }
    } */
    
    onIndividualChange = () => {

      this.setState({ isDateBlock: false, radioCheck: true });
    }

    onOrganizationChange = () => {

      this.setState({ isDateBlock: true, radioCheck: false });
    }

  userIDhandleChange = (event) => {
    let RegExpression = /^[(!@$-%^)a-zA-Z0-9 \s]*$/;  
    let inputUserIDVal = event.target.value.replace(/\s/g, "");
    if(inputUserIDVal.length>=5 && inputUserIDVal.length <= 20 && RegExpression.test(inputUserIDVal)){
      this.setState({userId: event.target.value,class:'',errorMsg:''});
    }else if(inputUserIDVal.length === 0){
      this.setState({class:'error',errorMsg: "Enter User ID."});
    }else{
      this.setState({class:'error',errorMsg: userIDMsg});
    }    
  }

    onMainSearch = () => {
        this.props.history.push('/csi/');
    }
    
    onReset = (event) => {
        document.getElementById("enrollForm").reset();
        event.preventDefault();
    }

    onEnroll = (event) =>{
        if(this.state.fullName.length === 0 && this.state.userId.length === 0){
            this.setState({class:'error',errorMsg: "Please enter details."});
        }else if(this.state.isEmailAccept === false){
            this.setState({class:'error',disableAccept:false,acceptBtn:'', errorMsg: Constants.ERROR_MSGS.ENROLL_ACCEPT_DEFAULT_ERROR});
        }else{
            this.props.history.push('/csi/myfa/profile',{
              investmentAccountType:this.state.investmentAccountType,
              firstName:this.state.firstName,
              lastName:this.state.lastName,
              dob:this.state.dob,
              socialNumber:this.state.socialNumber,
              clientNumber:this.state.clientNumber,
              emailId:this.state.emailId,
              unEnrolldisplay:'display-non',
              userID:this.state.userID,
              enrolledMsg:'Client successfully enrolled'
            })
        }
        event.preventDefault();    
    }
    
    onAccept = (event) =>{
        if(this.state.emailId.length>0){
            this.setState({isEmailAccept:true,class:'',errorMsg: ''});
            this.props.history.push('/csi/myfa/profile',{
              investmentAccountType:this.state.investmentAccountType,
              firstName:this.state.firstName,
              lastName:this.state.lastName,
              dob:this.state.dob,
              socialNumber:this.state.socialNumber,
              clientNumber:this.state.clientNumber,
              emailId:this.state.emailId,
              unEnrolldisplay:'display-non',
              userID:this.state.userID,
              enrolledMsg:'Client successfully enrolled'
              // enrolledMsg:'Client successfully enrolled'
            })
        }
        event.preventDefault();
    }

    render(){
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Enroll New Customer MYFA</h1>
        	</div> 
        <div className="bauForms">
           {/* <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p> */}
            <form className="enroll-new" id="enrollForm">
               <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
               <p><b>Investment Accounts</b></p>
               <span /*  onChange={this.investmentAccountTypeChg.bind(this)} */ >
               <input type='radio' checked={this.state.radioCheck} value='Individual' onChange={this.onIndividualChange}
                  name="investmentAccountType" className="enroll-radio" /> Individual<br></br>
                <input type='radio' value='Organization' onChange={this.onOrganizationChange}
                  name="investmentAccountType" className="enroll-radio"/> Organization
               </span>
               <br></br>
               <input type="text" placeholder="User ID" value={this.state.value} className="enroll" onChange={this.userIDhandleChange}/> 
               <input type="text" placeholder="First Name" value={this.state.value} className="enroll" onChange={this.firstNameHandlerChange}/> 
               <input type="text" placeholder="Last Name" value={this.state.value} className="enroll" onChange={this.lastNamehandleChange}/> 
               <input type="date" disabled={this.state.isDateBlock} placeholder="DOB (mm/dd/yyyy)" onChange={this.dobHandleChange} className="enroll"/> 
               
               {/* <DatePicker
    selected={this.state.startDate}
    onChange={this.handleChange}
    peekNextMonth
    showMonthDropdown
    showYearDropdown
    dropdownMode="select"
    placeholderText="DOB (mm/dd/yyyy)"
/> */}

               <input type="text" placeholder="SSN/TIN" value={this.state.value} className="enroll" onChange={this.SSNhandleChange}/> 
               <input type="text" placeholder="Client ID Number" className="enroll" onChange={this.ClientNumberHandleChange}/> 
               <input type="text" placeholder="Email Address" className="enroll email-accept" onChange={this.emailHandleChange}/> 
               <button type="button" disabled={this.state.disableAccept} onClick={this.onAccept}
                className={this.state.acceptBtn } >Accept</button>

               <div className="bttns">
                  <button onClick={this.onEnroll}>Enroll</button>
                  <button onClick={this.onReset}>Clear</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
                  {/* <span className="resetbtn" onClick={this.onReset}>Clear</span>  */}
               </div>   
            </form>

        </div>
        </div>
        )
    }
}

export default EnrollMyfa;