import React, { Component } from "react";
import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
import Header from "../../Header";
import Utils from '../../utils/Utils';
import * as Constants from '../../utils/Constants';

// redux action
import {loadAllData} from "./../../actions/dataAction"

//Redux imports
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import ConfigService from "../../api/ConfigService";
//import { dispatch } from "redux";
//import { loadAllData } from '../../actions/dataAction';

class Myfa extends Component {

constructor(props){
  	super(props);
    this.state = {
    	currentServicingType: 'MYFA',
        userId:'',
        guid:'',
        client_number:'',
        ssn:'',
        class:'error',
		errorMsg:'',
		validated: false,
		submitFlag: true,
		spinner: false,
		disableSSN: false,
		disableClientNumber: false,
		disableUserID: false,
		disableGUID: false
    }
	
	this.SSNhandleChange = this.SSNhandleChange.bind(this);
    this.clientNumberHandleChange = this.clientNumberHandleChange.bind(this);
    this.userIDhandleChange = this.userIDhandleChange.bind(this);
    this.guidHandleChange = this.guidHandleChange.bind(this);
	this.onSearch = this.onSearch.bind(this);
	this.onReset  = this.onReset.bind(this);
  	}


  	/**
   	 * Function to handle SSN input
     */
	SSNhandleChange = (event) => {

		//Not working
		// ConfigService.getSampleData().then(function(response){
		// 	if(response) {
		// 		console.log("RESP!!!!----", response);
		// 	}
		// })
		//console.log("data---", d);
		
		//console.log(this.props.allData);
		let allD = "bye";
		this.props.loadAllData(allD);

		// console.log(this.props.testData);
		// let data = "New Test Data";
		//getTestData(data);
		this.setState({ ssn: event.target.value });
		
		let valid = Utils.regExNumber(event.target.value);
		this.setState({ ssn: event.target.value });
		if(event.target.value.length) {
			this.disableInputs('ssn');
		} else {
			this.enableInputs('ssn');
		}
		if(valid) {
			if(event.target.value.length > 9) {
				this.setState({ class:'error', validated: false,
					errorMsg: Constants.ERROR_MSGS.SSN_LENGTH_ERROR });
			} else if (event.target.value.length) {
				this.setState({ class:'', errorMsg:'', validated: true });
			} else {
				this.setState({ class:'', errorMsg:'', validated: false });
			}
		} else {
			this.setState({ class: "error", validated: false, 
				errorMsg: Constants.ERROR_MSGS.SSN_VALIDATION_ERROR });
		} 
	}
	  
  	/**
	 * Function to handle Client Number input
	 */
  	clientNumberHandleChange = (event) => {
		this.setState({ client_number: event.target.value });
		let valid = Utils.regExNumber(event.target.value);
		if(event.target.value.length) {
			this.disableInputs('client_number');
		} else {
			this.enableInputs('client_number');
		}
		if(valid) {
			if(event.target.value.length > 12) {
				this.setState({ validated: false, class:'error', 
					errorMsg: Constants.ERROR_MSGS.CLIENTID_LENGTH_ERROR });
			} else if(event.target.value.length) {
				this.setState({ class:'', errorMsg:'', validated: true });
			} else {
				this.setState({ validated: false, class: "error", errorMsg: "" });
			}
		} else {
			this.setState({ validated: false, class: "error", 
				errorMsg: Constants.ERROR_MSGS.CLIENTID_VALIDATION_ERROR });
		}
    }

	
	/**
	 * Function to handle User ID input
	 */
	userIDhandleChange = (event) => {
		
		// Removing spaces
		let userIDValue = Utils.trimSpaces(event.target.value);
		this.setState({ userId: userIDValue });
		if(event.target.value.length) {
			this.disableInputs('user_id');
		} else {
			this.enableInputs('user_id');
		}
		let valid = Utils.regExAlphaNumeric(userIDValue);
		if(valid) {
			if (userIDValue.length >= 5 && userIDValue.length <= 20) {
				this.setState({ validated: true, class:'', errorMsg:''});
			} else if (userIDValue.length === 0) {
				this.setState({class: 'error', errorMsg: "Enter User ID.", validated: false });
			} else {
				this.setState({ class: 'error', validated: false, 
					errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
			}
		} else {
			this.setState({ validated: false });
		}
	}

	/**
	 * Function to handle GUID change
	 */
	guidHandleChange = (event) => {

		let gUIDVal = Utils.trimSpaces(event.target.value);
		this.setState({ guid: gUIDVal });
		if(event.target.value.length) {
			this.disableInputs('guid');
		} else {
			this.enableInputs('guid');
		}
		let valid = Utils.regExAlphaNumericOnly(event.target.value);
		if(valid) {
			if(event.target.value.length === 32) {
				this.setState({ validated: true, class:'', errorMsg:'' });
			} else {
				this.setState({ class: 'error', validated: false,
					errorMsg: Constants.ERROR_MSGS.GUID_LENGTH_ERROR });
			}
		} else {
			this.setState({ class:'error', validated: false, 
				errorMsg: Constants.ERROR_MSGS.GUID_VALIDATION_ERROR });
		}
		
  	}

	onSearch = (event) => {

		event.preventDefault();
		//this.setState({ spinner: true });
		if(this.state.ssn.length === 0 && this.state.client_number.length === 0 
			&& this.state.userId.length === 0 && this.state.guid.length === 0) {
				this.setState({ class: 'error', errorMsg: Constants.ERROR_MSGS.MYFA_NO_FIELD_ERROR });
		} else if(this.state.validated) {
			//this.setState({ submitFlag: false});
			if (this.state.userId.length > 0 || this.state.guid.length > 0) {
				this.props.history.push('/csi/myfa-ssn-guid-auth',{user:this.state.userId});
			} else if (this.state.ssn.length > 0) {
				this.props.history.push('/csi/myfa-client-number-auth',{ssn:this.state.ssn});
			} else if (this.state.client_number.length > 0) {
				this.props.history.push('/csi/myfa-snn-tin-number-auth',{client_number:this.state.client_number});
			}
		}
	}

  	onReset = (event) => {

    	//document.getElementById("userForm").reset();
		event.preventDefault();
		this.setState({ ssn: '', errorMsg:'',client_number: '', userId: '', guid: '', validated: false,
			disableSSN: false, disableClientNumber: false, disableUserID: false, disableGUID: false }); 
	}
	  
  	enrollNew = () => {
    	this.props.history.push('/csi/myfa-enroll-new')
	  }
	  
	/**
	 * Function to disable other inputs when one input is changed
	 */
	disableInputs = (value) => {

		switch(value) {
			case 'ssn':				this.setState({ disableClientNumber: true, disableUserID: true, disableGUID: true });
									break;
			case 'client_number': 	this.setState({ disableSSN: true, disableUserID: true, disableGUID: true});
									break;
			case 'user_id': 		this.setState({ disableSSN: true, disableClientNumber: true, disableGUID: true});
									break;
			case 'guid':			this.setState({ disableSSN: true, disableClientNumber: true, disableUserID: true});
									break;
		}
	}

	/**
	 * Function to enable other inputs when one input is reverted
	 */
	enableInputs = (value) => {

		switch(value) {
			case 'ssn':				this.setState({ disableClientNumber: false, disableUserID: false, disableGUID: false });
									break;
			case 'client_number': 	this.setState({ disableSSN: false, disableUserID: false, disableGUID: false});
									break;
			case 'user_id': 		this.setState({ disableSSN: false, disableClientNumber: false, disableGUID: false });
									break;
			case 'guid':			this.setState({ disableSSN: false, disableClientNumber: false, disableUserID: false });
									break;
		}
	}

  render() {
    return (
      <div>
		  {/*this.state.spinner && <Spinner />*/}
        <Header />
        <HeaderTab />
       <div className="bauSelect align-center">
          <strong>&raquo; </strong><b>Current Servicing Type</b> <strong>-</strong> {this.state.currentServicingType}            
       </div>
        <div className="bauForms">
           <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p>
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
           <br></br>
           {/* <p className={this.state.class}> {this.state.error} </p> */}
            <form id='userForm'>
				{ this.state.submitFlag && 
					<span>
						<input type="text" placeholder="SSN/TIN" value={this.state.ssn}
							disabled={this.state.disableSSN} onChange={this.SSNhandleChange} />
               			<span className='orColor'>Or</span> 
						<input type="text" placeholder="Client Number" value={this.state.client_number} 
							disabled={this.state.disableClientNumber} onChange={this.clientNumberHandleChange}/>
               			<span className='orColor'>Or</span> 
						<input type="text" placeholder="User ID" value={this.state.userId} 
							disabled={this.state.disableUserID} onChange={this.userIDhandleChange} />
               			<span className='orColor'>Or</span> 
						<input type="text" placeholder="GUID" value={this.state.guid} 
							disabled={this.state.disableGUID} onChange={this.guidHandleChange} />
               			<div className="bttns">
							<button onClick={this.onSearch}>Search</button>
							<button onClick={this.enrollNew}>Enroll New</button>
							<button onClick={this.onReset}>Clear</button>
							{/* <span className="resetbtn" onClick={this.onReset}>Clear</span>  */}
				   		</div>
					</span> }
            </form>

        </div>
        
      </div>
    );
  }
}
 
const mapStateToProps = (state) => {
	return {
		allData: state.allData
	}	
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadAllData
}, dispatch);

// mapStateToProps 

// mapDispatchToProps 
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Myfa);