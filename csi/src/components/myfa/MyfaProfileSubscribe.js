import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
import * as Constants from "../../utils/Constants";
class MyfaProfileSubscribe extends Component{

    constructor(props){
        super(props);
        this.state = {
            title:' MYFA Customer Information ',
            class:'',
            pass:'',
            errorMsg:'',
            isSubscribe:'Unsubscribe',
            unEnrolldisplay:'',
            changeUserMsg:'',
            investmentAccountType:'Individual',
            isDateBlock:false,
            userID:this.props.location.state.userID,
            acceptBtn:'btn-gray',

            emailId:'',
            clientNumber:'',
            socialNumber:'',
            firstName:'',
            lastName:'',
            dob:'',
            isEmailAccept:true,
            radioCheck: true

        };
    // this.investmentAccountTypeChg = this.investmentAccountTypeChg.bind(this);
    this.ClientNumberHandleChange = this.ClientNumberHandleChange.bind(this);
    this.SSNhandleChange = this.SSNhandleChange.bind(this);
    this.firstNameHandlerChange = this.firstNameHandlerChange.bind(this);
    this.lastNamehandleChange = this.lastNamehandleChange.bind(this);
    this.emailHandleChange = this.emailHandleChange.bind(this);
    this.dobHandleChange = this.dobHandleChange.bind(this);
    this.onIndividualChange = this.onIndividualChange.bind(this);
        this.onOrganizationChange = this.onOrganizationChange.bind(this);

    }

    dobHandleChange = (event) => {
        if(event.target.value.length > 0){
          this.setState({dob: event.target.value,class:'',errorMsg:''});
        }else{
          this.setState({class:'error',errorMsg: "DOB can not be empty"});
        }
      }
      
    lastNamehandleChange = (event) => {
        if(event.target.value.length >= 1){
          this.setState({lastName: event.target.value,class:'',errorMsg:''});
        }else{
          this.setState({class:'error',errorMsg: "Last name can not be empty"});
        }
      }

      emailHandleChange = (event) => {
        var emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let inputVal = event.target.value;
        if(inputVal.length>=5 && inputVal.length <= 254 && emailRegExp.test(inputVal)){
          this.setState({emailId: event.target.value,class:'',errorMsg:''});
          if(event.target.value ==='vverma14@ampf.com'){
            this.setState({class:'',isEmailAccept:false,errorMsg: ""});
            // this.setState({class:'error',isEmailAccept:false,acceptBtn:'',errorMsg: "You must accept the email address to proceed."});

        }else{
            this.setState({class:'',isEmailAccept:true,errorMsg: ""});
        }
        }else if(inputVal.length === 0){
          this.setState({class:'error',errorMsg: "Please enter Email ID",pass:''});
        }else{
          this.setState({class:'error',errorMsg: "Invalid Email ID",pass:''});
        }    
      }

      firstNameHandlerChange = (event) => {
        if(event.target.value.length >= 1){
          this.setState({firstName: event.target.value,class:'',errorMsg:''});
        }else{
          this.setState({class:'error',errorMsg: "First name can not be empty"});
        }
      }

    SSNhandleChange = (event) => {
        if(event.target.value.length <= 9){
          this.setState({socialNumber: event.target.value,class:'',errorMsg:''});
        }else{
          this.setState({class:'error',errorMsg: "SSN can't be more than 9 characters"});
        }
      }

  ClientNumberHandleChange = (event) => {
    if(event.target.value.length === 12){
      this.setState({clientNumber: event.target.value,class:'',errorMsg:''});
    }else{
      this.setState({class:'error',errorMsg: "Invalid Client ID. Client ID must contain 12 numbers"});
    }
  }

    /* investmentAccountTypeChg = (event) =>{
        this.setState({investmentAccountType:event.target.value})
        if(event.target.value ==='Individual'){
            this.setState({isDateBlock:false})
        }else{
            this.setState({isDateBlock:true})
        }
    } */

    onIndividualChange = () => {
        this.setState({ isDateBlock: false, radioCheck: true });
    }
  
    onOrganizationChange = () => {
        this.setState({ isDateBlock: true, radioCheck: false });
    }

    onMainSearch = () => {
        this.props.history.push('/csi/');
    }
    
    enrollNew = () => {
        this.props.history.push('/csi/myfa-enroll-new');
    }

    changeUserID = () => {
        this.props.history.push('/csi/myfa-change-user-id');
    }

    onAccept = (event) =>{
        if(this.state.emailId.length>0){
                this.setState({isEmailAccept:true,class:'success',errorMsg: 'Email ID Accepted'});
                this.props.history.push('/csi/myfa/profile',{
                investmentAccountType:this.state.investmentAccountType,
                firstName:this.state.firstName,
                lastName:this.state.lastName,
                dob:this.state.dob,
                socialNumber:this.state.socialNumber,
                clientNumber:this.state.clientNumber,
                emailId:this.state.emailId,
                unEnrolldisplay:'display-non',
                userID:this.state.userID,
                subscribeMsg:'Client successfully subscribed'
                });
                this.setState({acceptBtn:''});
        }
        event.preventDefault();
    }

    subscribe = (event) => {
        const data = {
            investmentAccountType:this.state.investmentAccountType,
            firstName:this.state.firstName,
            lastName:this.state.lastName,
            dob:this.state.dob,
            socialNumber:this.state.socialNumber,
            clientNumber:this.state.clientNumber,
            emailId:this.state.emailId,
            unEnrolldisplay:'display-non',
            userID:this.state.userID,
            subscribeMsg:'Client successfully subscribe'
        }
        if(this.state.firstName.length === 0 || this.state.lastName.length === 0){
            this.setState({class:'error',errorMsg: "Please enter details."});
            event.preventDefault();    
        }else  if(this.state.isEmailAccept === false){
            this.setState({class:'error',acceptBtn:'',errorMsg: Constants.ERROR_MSGS.ENROLL_ACCEPT_DEFAULT_ERROR});
        }else{
            this.props.history.push('/csi/myfa/profile',data);
        }

        // if(this.state.isSubscribe ==='Unsubscribe'){
        //     this.setState({class:'success',errorMsg: "Client successfully Unsubscribed",pass:''});
        //     if (window.confirm("Are you sure you want to unsubscribe")) { 
        //         this.setState({isSubscribe:'Subscribe'})
        //       }
        // }else if(this.state.isSubscribe ==='Subscribe'){
        //     if (window.confirm("Are you sure you want to subscribe")) { 
        //         this.setState({isSubscribe:'Unsubscribe'})
        //         this.setState({class:'success',errorMsg: "Client successfully subscribed",pass:''});
        //       }
        // }
    }

    onRestPassword = (event) =>{
        if (window.confirm("Are you sure you want to reset password?")) { 
            this.setState({class:'error',errorMsg:"Reset Password Successful - New password is: ",pass:'! 3 4 5 6 $ % #'});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();    
        
    }
    onUnenroll = (event) =>{
        if (window.confirm("Confirm unenroll from Total View.")) { 
            this.setState({class:'success',unEnrolldisplay:'display-non',errorMsg:"Success: client unenrolled from total view",pass:''});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();   
    }
    
    render(){
        let showResetPassBtnClass = (this.state.isSubscribe==='Unsubscribe')?'':'display-non'
        let resetMessage, message;
        let newUserID  = this.props.location.state?this.props.location.state.userID:this.state.userID;
        if(this.state.errorMsg.length>0){
            this.props.location.state.UnSubscribedMsg = '';
        }
        if(this.state.pass!=''){
            resetMessage = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }else if(this.state.errorMsg.length ==0 && this.props.location.state){
            if(this.props.location.state.msg){
                message = <MessageBar errorMsg={this.props.location.state.msg} className="success"/>
            }else if(this.props.location.state.UnSubscribedMsg){
                message = <MessageBar errorMsg={this.props.location.state.UnSubscribedMsg} className="success"/>
            }else{
                resetMessage = <MessageBar errorMsg={this.props.location.state.enrolledMsg} className="success"/>
            }
        }else if(this.state.pass==''){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
        }

        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> {this.state.title}</h1>
            </div>    
                <div className="profileForms">
        <table>
            <tbody>
        <tr className="profile-head-color">
            <th>Security Identity Data</th>
            <th></th>
            <td>Enrolled Services</td> 
        </tr>
        <tr>
            <td className="bold td1">User ID:</td>
            <td className="td2">{newUserID} </td> 
            <td className="td3">SSO Service , Message Center , Quicken</td> 
        </tr>
        <tr>
            <td className="bold td1">Status:</td>
            <td className="td2">Active</td> 
        </tr>
        <tr>
            <td className="bold td1">GUID:</td>
            <td className="td2">wertyuiosdfghjklxcvbn345678</td> 
        </tr>
        <tr>
            <td className="bold td1">Last Login:</td>
            <td className="td2">01/27/2018</td> 
        </tr>
        <tr>
            <td className="bold td1">Password Status:</td>
            <td className="td2">Active</td> 
        </tr>
        </tbody>
        </table>
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            <div className="profile-head-color">
                    <span className="profile-head-left">My Financial Account Information</span>
                    {/* <span className="profile-head-right">Total View Information</span> */}
            </div>
        {message}
        <div className="profile-head-color">
                    <span className="profile-head-left">Investment Accounts</span>
                    <span className="myfa-head-right">
                    {/* Total View Information */}
                        </span>
            </div>
            <div className="divTable">
                <table>
                    <tbody>
                    <tr>
                    <td /* onChange={this.investmentAccountTypeChg.bind(this)} */ className="bold td1">
                    <input type='radio' checked={this.state.radioCheck} value='Individual'
                        onChange={this.onIndividualChange} name="investmentAccountType"/> Individual<br></br>
                    <input type='radio' value='Organization' name="investmentAccountType"
                        onChange={this.onOrganizationChange} /> Organization
                    </td>
                    
                </tr>
                <tr>
                    <td className="bold td1">First Name:</td>
                    <td className="td2">
                    <input type="text" placeholder="" value={this.state.value} className="enroll" onChange={this.firstNameHandlerChange} />
                    </td> 
                </tr>
                <tr>
                    <td className="bold td1">Last Name:</td>
                    <td className="td2">
                    <input type="text" placeholder="" value={this.state.value} className="enroll" onChange={this.lastNamehandleChange} />
                    </td> 
                </tr>
                <tr>
                    <td className="bold td1">DOB: (dd/mm/yyyy)</td>
                    <td className="td2">
                    <input type="date" disabled={this.state.isDateBlock} placeholder="" value={this.state.value} className="enroll" onChange={this.dobHandleChange} />
                    </td> 
                </tr>
                
                {/* <tr>
                    <td className="bold td1">User Type:</td>
                    <td className="td2">
                        <input type="text" placeholder="" value={this.state.value} className="enroll" onChange={this.SSNhandleChange} />
                    </td> 
                    <td className="myfa-td3-enroll">
                       </td> 
                </tr> */}
                <tr>
                    <td className="bold td1">SSN/TIN:</td>
                    <td className="td2">
                    <input type="text" placeholder="" value={this.state.value} className="enroll" onChange={this.SSNhandleChange} />                    
                    </td> 
                </tr>
                <tr>
                    <td className="bold td1">Client ID Number:</td>
                    <td className="td2">
                        <input type="text" placeholder="" value={this.state.value} className="enroll" onChange={this.ClientNumberHandleChange} />
                    </td>                     
                </tr>
                <tr>
                    <td className="bold td1">Email:</td>
                    <td className="td2">
                        <input type="text" placeholder="" value={this.state.value} className="enroll" onChange={this.emailHandleChange} />
                    </td> 
                    <td className="myfa-td3">
                        <button className={this.state.acceptBtn} onClick={this.onAccept}>Accept</button>
                    </td> 
                </tr>

                </tbody>
                </table> 
                <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button className="myfa-sub-btn" onClick={this.subscribe}>
                                    {/* {this.state.isSubscribe} */}Subscribe
                                    </button>
                                    {/* <button onClick={this.changeUserID}>Change User ID</button> */}
                            </center>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
                {resetMessage}
                <form>

                 <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button className={showResetPassBtnClass} onClick={this.onRestPassword}>Reset Password</button>
                                    <button onClick={this.onMainSearch}>Main Search</button>
                                    <button onClick={this.enrollNew}>Enroll New Client</button>
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}

export default MyfaProfileSubscribe;