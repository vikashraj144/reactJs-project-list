import React, { Component } from "react";
import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
import Utils from "../../utils/Utils";
import * as Constants from '../../utils/Constants';

class Pap extends Component {

constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'PAP',
        emailId:'',
        ssnNo:'',
        class:'error',
        errorMsg:'',
        pass:'',
    		disableSSN: false,
		    disableEmail: false,
        validated: false,

    }
    
    this.emailHandleChange = this.emailHandleChange.bind(this);
    this.ssnHandleChange = this.ssnHandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }
 
  ssnHandleChange = (event) => {
    let disableEmailVal = (event.target.value.length)?true:false;
    this.setState({ ssnNo: event.target.value,disableEmail:disableEmailVal });
    
    let valid = Utils.regExNumber(event.target.value);
    if(valid) {
			if(event.target.value.length > 9) {
				this.setState({ class:'error', validated: false,
					errorMsg: Constants.ERROR_MSGS.SSN_LENGTH_ERROR });
			} else if (event.target.value.length) {
				this.setState({ class:'', errorMsg:'', validated: true });
			} else {
				this.setState({ class:'', errorMsg:'', validated: false });
			}
		} else {
			this.setState({ class: "error", validated: false, 
				errorMsg: Constants.ERROR_MSGS.SSN_VALIDATION_ERROR });
		} 
  }

  emailHandleChange(event) {
    let disableSnnVal = (event.target.value.length)?true:false;
		this.setState({ emailId: event.target.value,disableSSN:disableSnnVal,validated:false });
    var emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let inputVal = event.target.value;
    if(inputVal.length>=5 && inputVal.length <= 254 && emailRegExp.test(inputVal)){
      this.setState({validated:true,class:'',errorMsg:''});
    }else if(inputVal.length === 0){
      this.setState({class:'error',validated:false,errorMsg: Constants.ERROR_MSGS.ENTER_EMAIL_SSN,pass:''});
    }else{
      this.setState({class:'error',validated:false,errorMsg: Constants.ERROR_MSGS.INVALID_EMAIL,pass:''});
    }    
  }

  onSearch = (event) =>{
    event.preventDefault();    
    if(this.state.emailId.length === 0 && this.state.ssnNo.length === 0){
      this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.ENTER_EMAIL_SSN,pass:''});
      event.preventDefault();    
    // }else{
		} else if(this.state.validated) {
      this.props.history.push('/csi/pap/profile')
    }
  }

  onClear = (event) => {
    event.preventDefault();      
    this.setState({ emailId:'',ssnNo:'',disableSSN:false,disableEmail:false,errorMsg:''})
    // document.getElementById("userForm").reset();
  }

  render() {
    return (
      <div>
        <HeaderTab />
       <div className="bauSelect align-center">
          <strong>&raquo; </strong><b>Current Servicing Type</b> <strong>-</strong> {this.state.currentServicingType}            
       </div>
        <div className="bauForms">
           <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p>
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
<br></br>
              <form id='userForm'>
               <input type="text" placeholder="Email ID" disabled={this.state.disableEmail} value={this.state.emailId} onChange={this.emailHandleChange} /> 
               <span className='orColor'>Or</span>
               <input type="text" placeholder="SSN" disabled={this.state.disableSSN}  value={this.state.ssnNo} onChange={this.ssnHandleChange} />                
               <div className="bttns">
                  <button onClick={this.onSearch}>Search</button>
                  <button onClick={this.onClear}>Clear</button>
               </div>   
            </form>

        </div>
        
      </div>
    );
  }
}
 
export default Pap;