import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";

class PapProfile extends Component{

    constructor(props){
        super(props);
        this.state = {
            class:'',
            errorMsg:'',
            pass:''
        };
    }

    onMainSearch = () => {
        this.props.history.push('/csi/pap');
    }

    onRestPassword = (event) =>{
        if (window.confirm("Are you sure you want to reset password?")) { 
            this.setState({class:'error',errorMsg:"Reset Password Successful - New password is: ",pass:'! 3 4 5 6 $ % #'});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();    
        
    }

    render(){
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> PAP Customer Information</h1>
            </div>    

                <div className="profileForms">
                        {/* 1st tab start */}
<div>
<table>
  <tbody>
  <tr className="profile-head-color">
    <th>Security Identity Data</th>
    <th></th>
    <td>Enrolled Services</td> 
  </tr>
  <tr>
    <td className="bold td1">User ID:</td>
    <td className="td2">David</td> 
    <td className="td3">PAP, TR Admin Service</td> 
  </tr>
  <tr>
    <td className="bold td1">Status:</td>
    <td className="td2">Active</td> 
  </tr>
  <tr>
    <td className="bold td1">GUID:</td>
    <td className="td2">34567</td> 
  </tr>
  <tr>
    <td className="bold td1">Last Login:</td>
    <td className="td2">01/27/2018</td> 
  </tr>
  <tr>
    <td className="bold td1">Password Status:</td>
    <td className="td2">Temporary and Expired</td> 
  </tr>
  </tbody>
</table>
</div>
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            <div className="profile-head-color">
                    <span className="profile-head-left">Professional Alliance Program Profile's Information</span>
            </div>
            <div className="divTable">
            <h3>Partner's Information</h3>
{/* 2nd table */}
<table>
<tbody>
  <tr>
    <td className="bold partner-Info-td1 bold">First Name:</td>
    <td className="partner-Info-td2">David</td>
    <td className="bold partner-Info-td1">Last Name:</td>
    <td className="partner-Info-td2">Nadal</td> 
  </tr>
  <tr>
    <td className="bold partner-Info-td1 bold">E mail:</td>
    <td className="partner-Info-td2">David</td>
    <td className="bold partner-Info-td1">SSN</td>
    <td className="partner-Info-td2">*** ** 0017</td> 
  </tr>
  </tbody>
</table>
<br></br>
                <h3>Advisor's Information</h3>

<table>
  <tbody>
  <tr>
    <td className="bold partner-Info-td1 bold">Advisor ID:</td>
    <td className="partner-Info-td2">234567</td>
    <td className="bold partner-Info-td1">Name:</td>
    <td className="partner-Info-td2">55555555555</td> 
  </tr>
  <tr>
    <td className="bold partner-Info-td1 bold">Advisor First Name:</td>
    <td className="partner-Info-td2">Advisor</td>
    <td className="bold partner-Info-td1">Address 1:</td>
    <td className="partner-Info-td2">Complex</td> 
  </tr>

  
  <tr>
    <td className="bold partner-Info-td1 bold">Advisor Last Name:</td>
    <td className="partner-Info-td2">Advisor</td>
    <td className="bold partner-Info-td1">Address 2:</td>
    <td className="partner-Info-td2">Building</td> 
  </tr>
  <tr>
    <td className="bold partner-Info-td1 bold"></td>
    <td className="partner-Info-td2"></td>
    <td className="bold partner-Info-td1">City:</td>
    <td className="partner-Info-td2">Jumppark</td> 
  </tr>
  
  <tr>
  <td className="bold partner-Info-td1 bold"></td>
    <td className="partner-Info-td2"></td>
    <td className="bold partner-Info-td1">State:</td>
    <td className="partner-Info-td2">SC</td>  
  </tr>
  <tr>
    <td className="bold partner-Info-td1 bold"></td>
    <td className="partner-Info-td2"></td>
    <td className="bold partner-Info-td1">Zip:</td>
    <td className="partner-Info-td2">123456</td> 
  </tr>
  </tbody>
</table>
            </div>
            
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class} pass={this.state.pass}/>
                <form id="profileForm">
                 <div className="divTable">

                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    <button onClick={this.onMainSearch}>Main Search</button>
                                    <button onClick={this.onRestPassword}>Reset Password</button>

                                    {/* <span className="resetbtn" onClick={this.onReset}>Clear</span>                                      */}
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}

export default PapProfile;