import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
import Utils from '../../utils/Utils';
import * as Constants from '../../utils/Constants';

class EnrollSep extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentServicingType: 'CC90',
            userId:'',
            name:'',
            class:'error',
            errorMsg:''
        }
        this.userIDhandleChange = this.userIDhandleChange.bind(this);
        this.namehandleChange = this.namehandleChange.bind(this);
        }

    namehandleChange = (event) => {
        var valid = Utils.regExAlphabetsOnly(event.target.value);
        this.setState({name:event.target.value});
        if(valid){
            if(event.target.value.length === 0){
                this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.NAME_VALIDATION});
            }else{
                this.setState({class:'',errorMsg:''});
            }  
        }else{
            this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.NAME_INVALIDATION});
        }
    }  
    
    /**
	 * Function to handle User ID input
	 */
	userIDhandleChange = (event) => {
		// Removing spaces
    let userIDValue = Utils.trimSpaces(event.target.value);
    let disableGUIDVal = (event.target.value.length)?true:false;
		this.setState({ userId: userIDValue,disableGUID:disableGUIDVal });
        let valid = Utils.regExAlphaNumeric(userIDValue);
		if(valid) {
			if (userIDValue.length >= 5 && userIDValue.length <= 20) {
                this.setState({ validated: true, class:'', errorMsg:''});
                if(!Utils.regExOneAlphabetsAtleastOneNumber(userIDValue)){
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR, validated: false });
                }                
			} else if (userIDValue.length === 0) {
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION, validated: false });
			} else {
				this.setState({ class: 'error', validated: false, 
					errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
			}
		} else {
			this.setState({ validated: false });
		}
	}

    onMainSearch = () => {
        this.props.history.push('/csi/sep');
    }
    
    onReset = (event) => {
        event.preventDefault();    
        this.setState({name:'',userId:''})
    }

    onEnroll = (event) =>{
        if(this.state.name.length === 0 || this.state.userId.length === 0){
            this.setState({class:'error',errorMsg: "Please enter details."});
            event.preventDefault();    
          }else{
            this.props.history.push('/csi/sep/profile',{enrolledMsg:'Enroll operation Successful - New password is:',pass:' ! 3 4 5 6 $ % #'});
            event.preventDefault();    
          }
    }

    render(){
        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span>Enroll New Customer SEP</h1>
        	</div> 
        <div className="bauForms">
            <form className="enroll-new" id="enrollForm">
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
<br></br>
               <input type="text" placeholder="Name" className="enroll" value={this.state.name} onChange={this.namehandleChange}/> 
               <input type="text" placeholder="User ID" className="enroll" value={this.state.userId} onChange={this.userIDhandleChange}/> 
               <div className="bttns">
                  <button onClick={this.onEnroll}>Enroll</button>
                  <button onClick={this.onMainSearch}>Main Search</button>
                  <button onClick={this.onReset}>Clear</button>
               </div>   
            </form>
        </div>
        </div>
        )
    }
}

export default EnrollSep;