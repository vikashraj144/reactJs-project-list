import React, { Component } from "react";
import HeaderTab from "../include/HeaderTab";
import MessageBar from "../shaired/MessageBar";
import Utils from '../../utils/Utils';
import * as Constants from '../../utils/Constants';

import {loadSepFormData} from "./../../actions/dataAction"

//Redux imports
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
class Sep extends Component {
   
constructor(props){
  super(props);
    this.state = {
        currentServicingType: 'SEP',
        // userId:'',
        class:'error',
        errorMsg:'',
        validated:false,
		    sepForm : {}
    }    
    this.userIDhandleChange = this.userIDhandleChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  /* 
    intializing sepForm and setting in state
  */
  componentDidMount = () =>{
    let sepForm = this.state.sepForm;
    sepForm.userId = ''
    this.setState({sepForm});
  }
  /**
	 * Function to handle User ID input
	 */
	userIDhandleChange = (event) => {
    // Removing spaces
		// this.setState({ userId: event.target.value });
    
    let sepForm = this.state.sepForm;
		sepForm.userId = event.target.value;
		this.setState({sepForm})

    let userIDValue = Utils.trimSpaces(event.target.value);
    let disableGUIDVal = (event.target.value.length)?true:false;
		this.setState({disableGUID:disableGUIDVal });
    let valid = Utils.regExAlphaNumeric(userIDValue);
		if(valid) {
			if (userIDValue.length >= 5 && userIDValue.length <= 20) {
        // dispatching to redux
        this.props.loadSepFormData(this.state.sepForm);
        this.setState({ validated: true, class:'', errorMsg:''});
        if(Utils.regExNumber(userIDValue)){
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_VALIDATION_NUMBER_CHAR_LENGTH_ERROR, validated: false });
                }                
			} else if (userIDValue.length === 0) {
				this.setState({class: 'error', errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION, validated: false });
			} else {
				this.setState({ class: 'error', validated: false, 
					errorMsg: Constants.ERROR_MSGS.USER_ID_VALIDATION_ERROR });
			}
		} else {
			this.setState({ validated: false });
		}
	}

  onSearch = (event) =>{
    event.preventDefault();
    if(this.state.sepForm.userId.length === 0 ){
      this.setState({class:'error',errorMsg: Constants.ERROR_MSGS.USER_ONLY_VALIDATION});
    }else if(this.state.validated){
      this.props.history.push('/csi/sep/profile')
    }
  }

  onReset = (event) => {
    event.preventDefault();  
    let sepForm = this.state.sepForm;
    sepForm.userId = '';  
    this.setState({sepForm,errorMsg:''});
  }

  onEnroll = () => {
    this.props.history.push('/csi/sep-enroll-new')
  }

  render() {
    return (
      <div>
        <HeaderTab />
       <div className="bauSelect align-center">
          <strong>&raquo; </strong><b>Current Servicing Type</b> <strong>-</strong> {this.state.currentServicingType}            
       </div>
        <div className="bauForms">
           <p className="align-center"><strong>&laquo; </strong>Enter information as supplied by customer in the fields below<strong> &raquo; </strong></p>
           <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
           <br></br>
           {/* <p className={this.state.class}> {this.state.error} </p> */}
              <form id='userForm'>
               <input type="text" placeholder="User ID" value={this.state.sepForm.userId} onChange={this.userIDhandleChange} autoFocus/> 
               <div className="bttns">
                  <button onClick={this.onSearch}>Search</button>
                  <button onClick={this.onEnroll}>Enroll New</button>
                  <button onClick={this.onReset}>Clear</button>

                  {/* <span className="resetbtn" onClick={this.onReset}>Clear</span>  */}
               </div>   
            </form>

        </div>
        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
	return {
		sepFormData: state.sepFormData
	}	
};

const mapDispatchToProps = (dispatch) => bindActionCreators({
	loadSepFormData
}, dispatch);
// export default Sep;
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Sep);