import React,{Component} from "react";
import MessageBar from "../shaired/MessageBar";
// import InputMessageBar from "../shaired/InputMessageBar";

class SepProfile extends Component{

    constructor(props){
        super(props);
        this.state = {
            class:'',
            errorMsg:'',
            isSubscribe:'Subscribe',
            pass:''
        };
    }

    onMainSearch = () => {
        this.props.history.push('/csi/sep');
    }
    
    enrollNew = () => {
        this.props.history.push('/csi/sep-enroll-new');
    }

    subscribe = () => {
        if(this.state.isSubscribe ==='Unsubscribe'){
            if (window.confirm("Are you sure you want to unsubscribe the user ?")) { 
                this.setState({class:'success',errorMsg: "Unsubscribe operation Successful"});
                this.setState({isSubscribe:'Subscribe'})
              }
            this.setState({isSubscribe:'Subscribe'})
        }else if(this.state.isSubscribe ==='Subscribe'){
                this.setState({isSubscribe:'Unsubscribe',class:'success',errorMsg: "Subscribe operation Successful"})
        }
    }

    toggleSubscribe = () => {
        this.setState({class:'',errorMsg: ""});
    }

    onRestPassword = (event) =>{
        if (window.confirm("Are you sure you want to reset password?")) { 
            this.setState({class:'error',errorMsg:"Reset Password Successful - New password is: ",pass:'! 3 4 5 6 $ % #'});
          }else{
            this.setState({class:"",errorMsg:"",pass:""});
          }
          event.preventDefault();    
    }
    
    render(){

        const isError = this.state.class;
        let message,enrolledMsgBar;
        if(isError === 'success'){
            message = <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/>
            // message = <InputMessageBar onClick={this.toggleSubscribe} errorMsg={this.state.errorMsg} class={this.state.class}/>
        }else if(isError === 'error'){
            enrolledMsgBar = <MessageBar errorMsg={this.state.errorMsg} pass={this.state.pass} className={this.state.class}/>
        }
        else if(this.state.errorMsg.length ==0 && this.props.location.state){
            enrolledMsgBar = <MessageBar errorMsg={this.props.location.state.enrolledMsg} pass={this.props.location.state.pass} className="success"/>
        }

        return(
        <div>
            <div className="titleBox">
                <h1><span>&raquo;</span> SEP Customer Information</h1>
            </div>    

                <div className="profileForms">
                        {/* 1st tab start */}
                        <table>
                    <tbody>
                    <tr className="profile-head-color">
                        <th>Security Identity Data</th>
                        <th></th>
                        <td>Enrolled Services</td> 
                    </tr>
                    <tr>
                        <td className="bold td1">User ID:</td>
                        <td className="td2">David</td> 
                        <td className="td3">SSO Service , Seminar Application , SEP</td> 
                    </tr>
                    <tr>
                        <td className="bold td1">Status:</td>
                        <td className="td2">Active</td> 
                    </tr>
                    <tr>
                        <td className="bold td1">QUID:</td>
                        <td className="td2">34567</td> 
                    </tr>
                    <tr>
                        <td className="bold td1">Last Login:</td>
                        <td className="td2">01/27/2018</td> 
                    </tr>
                    <tr>
                        <td className="bold td1">Password Status:</td>
                        <td className="td2">Temporary and Expired</td> 
                    </tr>
                    </tbody>
                    </table>
                    
                </div>
            {/* 2nd tab start */}
        <div className="profileForms">
            <div className="profile-head-color">
                    <span className="profile-head-left">Service Functions</span>
            </div>
           {/* <MessageBar errorMsg={this.state.errorMsg} className={this.state.class}/> */}
           {message}
            <div className="divTable">
            
                <div className="divTableBody">
                                    <div className="divTableRow">
                                        <div className="divTableCell service-function">SEP</div>
                                        <div className="divTableCell">
                                        <button onClick={this.subscribe}>{this.state.isSubscribe}</button>
                                        </div>
                                    </div>
                </div>
            </div>
        </div>
                {/* 2nd tab end */}
                 {/* 3rd tab start */}
        <div className="profileForms">
                <div className="profile-head-color">
                    <span className="profile-head-left">Security Functions</span>
                </div>
                {enrolledMsgBar}
                <form>

                 <div className="divTable">
                    <div className="divTableBody">
                        <div className="divTableRow">
                            <center>
                                    {/* <button onClick={this.deleteUserID}>Delete User ID</button> */}
                                    <button onClick={this.onRestPassword}>Reset Password</button>

                                    <button onClick={this.onMainSearch}>Main Search</button>
                                    <button onClick={this.enrollNew}>Enroll New</button>
                            </center>
                        </div>
                    </div>
                </div>
                </form>
        </div>
                {/* 3rd tab end */}
      </div>
        );
    }
}

export default SepProfile;