import React from "react";

const InputMessageBar = (props) => {
    return (
        <div className='error'>
        {props.errorMsg}
        <button onClick={props.onClick}>OK</button>
        <button>Cancel</button>
        </div>
    )
}

export default InputMessageBar;