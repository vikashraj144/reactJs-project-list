import axios from 'axios';
import * as Constants from '../utils/Constants';

export default class ConfigService {


    /**
     * Function to get SMSESSION at local
     */
    static getSession = () => {
        
        axios.get( Constants.e1BaseURL+'/testsmcsi?username=ssaini115&password=Fri@2019')
        .then((response) =>  {
            window.sessionStorage.setItem("SMSESSION", response.data.SMSESSION);
            getToken();
        })
        .catch(function (error) {
            console.log("Error fetching SMSESSION---", error);
        });
    }

    /**
     * Function to getToken using SMSESSION
     */
   /*  static getToken = () => {
    
        let url = Constants.e1BaseURL+'/csi/v1/api/token';
        fetch(url,
        {
            method: `POST`,
            credentials: `include`,
            headers: {  'Content-Type': 'application/x-www-form-urlencoded',
                        'clientId': Constants.e1clientId,
                        'clientSecret': Constants.e1clientSecret,
                    },
        }).then((res) => {
            window.sessionStorage.setItem("apiToken", res.headers.get('apitoken'));    
        }).catch((error) => {
            console.log("Error fetching apiToken--", error);
        });
    
    } */

    static getHeaderConfig = () => {
        return {
            headers: { 
            'Content-Type': 'application/x-www-form-urlencoded',
            apiToken:'1232132apiToken12321'
          },
            responseType: 'blob'
          };
    }

    static getSampleData = () => {
        axios.get(`https://reqres.in/api/users?page=2`)
        .then((response) => {
            console.log("-----",response.data);
            return response.data;
        })
        .catch(function (error) {
            return error;
        })
    }
}

/**
 * Function to fetch apiToken only at local
 */
const getToken = () => {
    
    let url = Constants.e1BaseURL+'/csi/v1/api/token';
    fetch(url,
    {
        method: `POST`,
        //credentials: `include`,
        headers: {  'Content-Type': 'application/x-www-form-urlencoded',
                    'clientId': Constants.e1clientId,
                    'clientSecret': Constants.e1clientSecret,
                    'SMSESSION': window.sessionStorage.getItem("SMSESSION")
                },
    }).then((res) => {
        window.sessionStorage.setItem("apiToken", res.headers.get('apitoken'));    
    }).catch((error) => {
        console.log("Error fetching apiToken--", error);
    });
}